package com.vule.toeic365.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.util.Base64
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.vule.toeic365.MainApplication
import com.vule.toeic365.R
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.math.cos

class Utils {
    @Suppress("DEPRECATION")
    @SuppressLint("RestrictedApi")
    companion object {
        fun removeShiftMode(view: BottomNavigationView) {
            val menuView = view.getChildAt(0) as BottomNavigationMenuView
            try {
                val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
                shiftingMode.isAccessible = true
                shiftingMode.setBoolean(menuView, false)
                shiftingMode.isAccessible = false
                for (i in 0 until menuView.childCount) {
                    val item = menuView.getChildAt(i) as BottomNavigationItemView
                    item.setShifting(false)
                    // set once again checked value, so view will be updated
                    item.setChecked(item.itemData.isChecked)
                }
            } catch (e: Throwable) {
                Timber.tag("BottomNav").e(e)
            } catch (e: IllegalAccessException) {
                Timber.tag("BottomNav").e(e)
            }
        }

        fun hideSoftKeyboard(activity: Activity) {
            val view = activity.currentFocus
            if (view != null) {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        fun doShare(message: String = "Put whatever you want"): Intent {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, setHtmlText(message))
            return intent
        }

        fun makePhoneCall(context: Context, phoneNumber: String) {
            val phoneCallIntent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null))
            context.startActivity(phoneCallIntent)
        }

        fun changeDrawableColor(
            context: Context = MainApplication.getInstance().applicationContext,
            icon: Int,
            newColor: Int
        ): Drawable? {
            val mDrawable = ContextCompat.getDrawable(context, icon)?.mutate()
            mDrawable?.colorFilter =
                PorterDuffColorFilter(ContextCompat.getColor(context, newColor), PorterDuff.Mode.SRC_ATOP)
            return mDrawable
        }

        fun getSpannableString(fullText: String?, subText: String?, what: Any?): SpannableString? {
            if (fullText == null || subText == null) return null
            val subTextIndex = fullText.indexOf(subText)
            if (subTextIndex > -1) {
                val spanText = SpannableString(fullText)
                spanText.setSpan(
                    what,
                    subTextIndex,
                    subTextIndex.plus(subText.length ),
                    Spannable.SPAN_EXCLUSIVE_INCLUSIVE
                )
                return spanText
            }
            return null
        }

        fun getDimension(resourceId: Int): Int {
            return MainApplication.getInstance().applicationContext.resources.getDimensionPixelOffset(resourceId)
        }

        fun getHorizontalPaddingFromCardView(
            cornerRadius: Int = getDimension(R.dimen._10sdp),
            elevationCard: Int = getDimension(R.dimen._10sdp)
        ): Int {
            val context = MainApplication.getInstance().applicationContext
            return (elevationCard + (1 - cos(45.0)) * cornerRadius).toInt()
        }

        fun getVerticalPaddingFromCardView(
            cornerRadius: Int = getDimension(R.dimen._10sdp),
            elevationCard: Int = getDimension(R.dimen._10sdp)
        ): Int {
            val context = MainApplication.getInstance().applicationContext
            return (elevationCard * 1.5 + (1 - cos(45.0)) * cornerRadius).toInt()
        }

        fun getString(stringId: Int): String {
            val context = MainApplication.getInstance().applicationContext
            return getStringByLocale(context, stringId)
        }

        fun getStringByLocale(context: Context, stringId: Int): String {
            val config = context.resources.configuration
            config.locale = Locale(SharedPreferenceHelper.getInstance(context).languageCode)
            val newResource = Resources(context.assets, context.resources.displayMetrics, config)
            return newResource.getString(stringId)
        }

        fun getStatusBarHeight(activity: Activity): Int {
            var result = 0
            val resourceId = activity.resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = activity.resources.getDimensionPixelSize(resourceId)
            }
            return result
        }

        fun setStatusBarColored(activity: Activity) {
            val w = activity.window
            w.setFlags(
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
            )
            val view = View(activity)
            view.layoutParams =
                FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            view.layoutParams.height = getStatusBarHeight(activity)
            (w.decorView as ViewGroup).addView(view)
            view.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimary))
        }

        fun setHtmlText(bodyData: String?): Spanned {
            return if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                Html.fromHtml(bodyData, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(bodyData)
            }
        }

        fun getDeviceWidth(activity: Activity?): Int {
            val displayMetrics = DisplayMetrics()
            activity?.windowManager?.defaultDisplay?.getMetrics(displayMetrics)
            return displayMetrics.widthPixels
        }

        fun getScreenHeight(): Int {
            return Resources.getSystem().displayMetrics.heightPixels
        }

        fun getColor(id: Int): Int {
            return ContextCompat.getColor(MainApplication.getInstance().applicationContext, id)
        }

        fun getDrawable(id: Int): Drawable? {
            return ContextCompat.getDrawable(MainApplication.getInstance().applicationContext, id)
        }

        fun encodeToBase64(image: Bitmap?, compressFormat: Bitmap.CompressFormat, quality: Int): String {
            val byteArrayOS = ByteArrayOutputStream()
            image?.compress(compressFormat, quality, byteArrayOS)
            return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.NO_WRAP)
        }

        fun decodeBase64(input: String?): Bitmap {
            val decodedByte: ByteArray = Base64.decode(input, 0)
            return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.size)
        }

        fun isPermissionGranted(
            grantPermissions: Array<String>, grantResults: IntArray,
            permission: String
        ): Boolean {
            for (i in grantPermissions.indices) {
                if (permission == grantPermissions[i]) {
                    return grantResults[i] == PackageManager.PERMISSION_GRANTED
                }
            }
            return false
        }
    }
}