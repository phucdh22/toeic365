package com.vule.toeic365.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.cardview.widget.CardView
import androidx.databinding.DataBindingUtil
import com.vule.toeic365.R
import com.vule.toeic365.databinding.LayoutConfirmDialogBinding
import com.vule.toeic365.databinding.LayoutLoadingBinding
import io.reactivex.functions.Consumer

object DialogUtils {

    fun progressDialog(activity: Activity?): Dialog? {
        val dialog = createDialog(activity, R.layout.dialog_progress)
        dialog?.setCancelable(false)
        val window = dialog?.window
        window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    fun showToastDialog(context: Context?, message: String?) {
//        if (message == null) return
//        val dialog = createDialog(context, R.layout.layout_toast)
//        val tvMessage = dialog?.findViewById<TextView>(R.id.tv_toast_message)
//        tvMessage?.text = message
//        dialog?.setCanceledOnTouchOutside(true)
//        dialog?.setCancelable(true)
//
//        val window = dialog?.window
//        window?.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
//        window?.setFlags(WindowManager.LayoutPara ms.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL)
//        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//        window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
//        val handler = Handler()
//        // Auto dismiss
//        handler.postDelayed({ dialog?.dismiss() }, 2500)
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show()
    }

    private fun createDialog(context: Context?, layout: Int): AlertDialog? {
        if (context == null) return null
        val alertDialogBuilder = AlertDialog.Builder(context)
        alertDialogBuilder.setView(layout)
        return alertDialogBuilder.show()
    }

    fun showConfirmDialog(context: Context?, onLeaveConsumer: Consumer<Unit>){
        if(context == null) return
        val dialog = AlertDialog.Builder(context)
        val binding = DataBindingUtil.inflate<LayoutConfirmDialogBinding>(
            LayoutInflater.from(context),
            R.layout.layout_confirm_dialog,
            null,
            false
        )
        dialog.setView(binding.root)
        val alert = dialog.show()
        alert?.setCanceledOnTouchOutside(true)
        alert?.setCancelable(true)

        alert?.window?.setBackgroundDrawableResource(R.color.transparent)

        binding.btnStay?.setOnClickListener {
            alert.dismiss()
        }
        binding.btnLeave?.setOnClickListener {
            onLeaveConsumer.accept(Unit)
        }
    }
    fun showLoadingDialog(context: Context) : AlertDialog {
        val alertDialogBuilder = AlertDialog.Builder(context)
        val binding = DataBindingUtil.inflate<LayoutLoadingBinding>(
            LayoutInflater.from(context),
            R.layout.layout_loading,
            null,
            false
        )
        alertDialogBuilder.setView(binding.root)
        val alert = alertDialogBuilder.show()
        alert.window?.setBackgroundDrawableResource(R.color.transparent)
        return alert
    }
}