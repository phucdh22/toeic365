package com.vule.toeic365.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.vule.toeic365.enum.LanguageType
import java.util.*

/**
 * this is manager class that support manage settings of app.
 * @author Sang
 */
class SharedPreferenceHelper private constructor(private val context: Context) {
    /**
     * get current doing preference
     * @return
     */
    private val preference: SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)
    private val gson: Gson? = null

    var selectedAvatarBitmapPath: String?
        get() = preference.getString(AVATAR_PATH, "")
        set(value) = preference.edit().putString(AVATAR_PATH, value).apply()

    var userToken: String?
        get() = preference.getString(USER_TOKEN, "")
        set(value) {
            val newValue = if (value?.isEmpty() == true) value else "bearer $value"
            preference.edit().putString(USER_TOKEN, newValue).apply()
        }

    var isKeepMeLogin: Boolean
        get() = preference.getBoolean(KEEP_ME_LOGIN, false)
        set(value) = preference.edit().putBoolean(KEEP_ME_LOGIN, value).apply()

    var isFirstRun: Boolean
        get() = preference.getBoolean(FIRST_RUN, true)
        set(value) = preference.edit().putBoolean(FIRST_RUN, value).apply()

    var languageCode: String?
        get() = preference.getString(
            LANGUAGE_CODE,
            if (Locale.getDefault().language != LanguageType.VN.value) LanguageType.EN.value else LanguageType.VN.value
        )
        set(value) = preference.edit().putString(LANGUAGE_CODE, value).apply()

    var isChangedLanguage: Boolean
        get() = preference.getBoolean(IS_CHANGED_LANGUAGE, false)
        set(value) = preference.edit().putBoolean(IS_CHANGED_LANGUAGE, value).apply()

    var deviceToken: String?
        get() = preference.getString(DEVICE_TOKEN, "")
        set(value) = preference.edit().putString(DEVICE_TOKEN, value).apply()
    /**
     * support get value from key
     * @return
     */
    /**
     * support track custom data
     * @return
     */

    companion object {
        private var mInstance: SharedPreferenceHelper? = null
        private const val AVATAR_PATH = "SharedPreferenceHelper.AVATAR_PATH"
        private const val USER_TOKEN = "SharedPreferenceHelper.USER_TOKEN"
        private const val KEEP_ME_LOGIN = "SharedPreferenceHelper.KEEP_ME_LOGIN"
        private const val FIRST_RUN = "SharedPreferenceHelper.FIRST_RUN"
        private const val LANGUAGE_CODE = "SharedPreferenceHelper.LANGUAGE_CODE"
        private const val IS_CHANGED_LANGUAGE = "SharedPreferenceHelper.IS_CHANGED_LANGUAGE"
        private const val DEVICE_TOKEN = "SharedPreferenceHelper.DEVICE_TOKEN"
        private const val USER_INFO = "USER_INFO"

        /**
         * get settings instance
         * @param context
         * @return
         */
        fun getInstance(context: Context): SharedPreferenceHelper {
            if (mInstance == null) {
                mInstance = SharedPreferenceHelper(context)
            }
            return mInstance as SharedPreferenceHelper
        }
    }

}