package com.vule.toeic365.binding

import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.textfield.TextInputLayout
import com.vule.toeic365.MainApplication
import com.vule.toeic365.R
import com.vule.toeic365.utils.Utils
import de.hdodenhof.circleimageview.CircleImageView
import android.view.View


@BindingAdapter("bind:textError")
fun setTextError(textInput: TextInputLayout, error: String?) {
    if (error != null) {
        textInput.error = error
        textInput.isErrorEnabled = true
    } else textInput.isErrorEnabled = false
}

@BindingAdapter("bind:loadAvatar")
fun loadAvatar(civ: CircleImageView, imageBase64: String?) {
    if (imageBase64.isNullOrEmpty() || imageBase64 == " null") {
        Glide.with(MainApplication.getInstance().applicationContext).load(
            R.drawable.defautlavatar
        ).into(civ)
    } else {
        Glide.with(MainApplication.getInstance().applicationContext).load(Utils.decodeBase64(imageBase64)).into(civ)
    }
}