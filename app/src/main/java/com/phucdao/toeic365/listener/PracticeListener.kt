package com.vule.toeic365.listener

interface PracticeListener {
    fun onPlayClicked(position: Int)
    fun onReviewClicked(position: Int)
    fun onReplayClicked(position: Int)
}