package com.vule.toeic365.listener

interface PracticeSubmitListener {
    fun onQuestionClicked(position: Int)
}