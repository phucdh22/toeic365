package com.vule.toeic365.listener

interface PracticeDetailListener{
    fun onUserAnswer(position: Int,questionId: Int, answerId: Int)
}