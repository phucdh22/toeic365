package com.vule.toeic365

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import com.vule.toeic365.base.LocaleManager
import com.vule.toeic365.constant.Constant
import io.realm.Realm
import io.realm.RealmConfiguration

class MainApplication: Application() {

    companion object {

        private lateinit var mInstance: MainApplication

        fun getInstance(): MainApplication {
            return mInstance
        }
        var localeManager: LocaleManager? = null

        const val TAG = "MainApplication"
    }
    var activityStack = arrayListOf<String>()

    override fun attachBaseContext(base: Context) {
        localeManager = LocaleManager()
        super.attachBaseContext(localeManager?.setLocale(base));
    }

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        initRealm()
        initActivityLifeCycle()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        localeManager?.setLocale(this);
    }

    private fun initRealm() {
        Realm.init(applicationContext)
        val realmConfiguration = RealmConfiguration.Builder()
            .name(Constant.REALM_DB_NAME)
            .schemaVersion(Constant.REALM_SCHEMA_VERSION)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(realmConfiguration)
    }

    private fun initActivityLifeCycle() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks{
            override fun onActivityPaused(activity: Activity?) {
            }

            override fun onActivityResumed(activity: Activity?) {
            }

            override fun onActivityStarted(activity: Activity?) {
            }

            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
            }

            override fun onActivityStopped(activity: Activity?) {
            }

            override fun onActivityDestroyed(activity: Activity?) {
                if (activity != null) activityStack.remove(activity::class.java.simpleName)
            }

            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
                if (activity != null) {
                    activityStack.add(activity::class.java.simpleName)
                }
            }
        })
    }
}