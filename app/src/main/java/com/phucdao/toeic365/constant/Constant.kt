package com.vule.toeic365.constant


object Constant {

    // Realm
    const val REALM_DB_NAME = "MajorComplex.realm"
    const val REALM_SCHEMA_VERSION = 1L
    // Preference
    const val CURRENT_USER_EMAIL_KEY = "CURRENT_USER_EMAIL_KEY"
    const val CURRENT_PART_ID = "CURRENT_PART_ID"
    const val CURRENT_PRACTICE_ID = "CURRENT_PRACTICE_ID"

    val mPracticeIdField = "practiceId"
    val mPartIdField = "partId"
}