package com.vule.toeic365.base

import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.vule.toeic365.MainApplication
import com.vule.toeic365.utils.SharedPreferenceHelper

open class BaseViewModel : ViewModel() {

    protected val TAG = javaClass.simpleName

    var mUserManager: UserManager? = null

    var mActivityNavigator: ActivityNavigator? = null

    var isFullScreen = ObservableField(true)

    private var deepLinkViewModel: Uri? = null

    val mSharedPreference
            by lazy { SharedPreferenceHelper.getInstance(MainApplication.getInstance().applicationContext) }

    open fun onReady() {}

    fun getTextWatcher(observableField: ObservableField<String>): TextWatcher {
        return object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                observableField.set(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {
            }

        }
    }

    fun makeToast(message: String) {
        Toast.makeText(
            MainApplication.getInstance().applicationContext,
            message,
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun onCleared() {
        mUserManager = null
        super.onCleared()
    }
}