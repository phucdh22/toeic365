package com.vule.toeic365.base

class UserManager private constructor(private val mActivity: BaseActivity) {

    companion object {
        private var mInstance: UserManager? = null

        fun getInstance(activity: BaseActivity?): UserManager {
            if (mInstance == null && activity != null) {
                mInstance = UserManager(activity)
            }
            return mInstance as UserManager
        }

    }

}