package com.vule.toeic365.base

import android.graphics.drawable.Drawable
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import io.reactivex.functions.Consumer

class AppBarViewModel : BaseObservable() {

    @Bindable
    val mTopLeftIcon = ObservableField<Drawable>()

    @Bindable
    val mTopTightIcon = ObservableField<Drawable>()

    @Bindable
    val mTitle = ObservableField<String>("")

    @Bindable
    val mTitleVisibility = ObservableField<Int>(View.GONE)

    @Bindable
    val mCenterIconVisibility = ObservableField<Int>(View.VISIBLE)

    @Bindable
    var mTopLeftVisibility = ObservableField<Int>(View.VISIBLE)

    @Bindable
    var mTopRightVisibility = ObservableField<Int>(View.VISIBLE)

    @Bindable
    var mBackgroundColor = ObservableField<Drawable>()

    var mOnTopLeftClickedConsumer: Consumer<Unit>? = null
    var mOnTopRightClickedConsumer: Consumer<Unit>? = null


    fun onTopLeftClicked(){
        mOnTopLeftClickedConsumer?.accept(Unit)
    }
    fun onTopRightClicked(){
        mOnTopRightClickedConsumer?.accept(Unit)
    }
}