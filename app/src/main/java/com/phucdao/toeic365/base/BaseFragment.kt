package com.vule.toeic365.base

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {
    open val TAG = javaClass.simpleName

    protected var mActivity: BaseActivity? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as? BaseActivity
    }
    override fun onResume() {
        super.onResume()
        setupActionBar()
    }

    open fun setupActionBar() {
        mActivity?.setNewLocale(mActivity?.mSharedPreference!!.languageCode)
    }

    open fun onPopStackChanged() {
        setupActionBar()
    }


    open fun doWantToFinishActivity(): Boolean {
        return false
    }

}