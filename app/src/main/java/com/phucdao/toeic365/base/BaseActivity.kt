package com.vule.toeic365.base

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.SystemClock
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.appbar.AppBarLayout
import com.vule.toeic365.MainApplication
import com.vule.toeic365.utils.DialogUtils
import com.vule.toeic365.utils.SharedPreferenceHelper
import com.vule.toeic365.utils.Utils
import io.reactivex.functions.Consumer
import timber.log.Timber


@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity(), FragmentManager.OnBackStackChangedListener {

    open val TAG = javaClass.simpleName

    var currentCountInStack = 0

    val mUserManager: UserManager
        get() = UserManager.getInstance(this)
    var mCurrentFragment: BaseFragment? = null
    var mFragmentAsContent: Fragment? = null

    private var mIsActivityRunning = true

    protected var mShowFullScreen: Boolean = false

    val mSharedPreference by lazy { SharedPreferenceHelper.getInstance(this) }

    protected var mAppBar: AppBarLayout? = null
    protected var mToolbar: Toolbar? = null
    var mAppBarViewModel: AppBarViewModel = AppBarViewModel()

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(MainApplication.localeManager?.setLocale(base))
        Timber.tag(TAG).d("attachBaseContext")
    }

    override fun onStop() {
        hideProgressDialog()
        super.onStop()
    }

    override fun onDestroy() {
        mIsActivityRunning = false
        hideSoftKeyboard()
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
        mIsActivityRunning = true
        setNewLocale(mSharedPreference.languageCode)
    }

    fun isRunning(): Boolean {
        return mIsActivityRunning
    }

    override fun onBackPressed() {
        Utils.hideSoftKeyboard(this)
        if (mCurrentFragment is BaseFragment && mCurrentFragment?.doWantToFinishActivity() == true) {
            finish()
            return
        }

        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
            updateCurrentFragment()
            return
        }
        super.onBackPressed()
    }

    fun updateCurrentFragment() { // only call in ActivityNavigator
        val fragmentManager = supportFragmentManager
        val fragment: Fragment? = if (fragmentManager.backStackEntryCount > 0) {
            val fragmentTag = fragmentManager.getBackStackEntryAt(fragmentManager.backStackEntryCount - 1).name
            fragmentManager.findFragmentByTag(fragmentTag)
        } else {
            try {
                fragmentManager.fragments[0]
            }catch (e: IndexOutOfBoundsException){
                null
            }
        }
        if (fragment is BaseFragment) {
            mCurrentFragment = fragment
        }
        mCurrentFragment?.onPopStackChanged()
    }

    override fun onBackStackChanged() {
        if (isMultiClicked() || currentCountInStack < supportFragmentManager.backStackEntryCount) return
        currentCountInStack = supportFragmentManager.backStackEntryCount
        Timber.tag("TEST_MULTI_CLICKED").d(System.currentTimeMillis().toString())
        updateCurrentFragment()
    }

    open fun hideSoftKeyboard() {
        val view = currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private var lastClickTime: Long = 0

    private var mLongClickedTime = 50
    // preventing double, using threshold of 700 ms
    open fun isMultiClicked(): Boolean {
        val now = SystemClock.elapsedRealtime()
        if (now - lastClickTime < mLongClickedTime) {
            Timber.tag(TAG).w(String.format("onClick Clicked too quickly: " + (now - lastClickTime) + " ms"))
            return true
        }
        Timber.tag(TAG).i("onClick Clicked too quickly pass: $mLongClickedTime")
        lastClickTime = now
        return false
    }


    private var progressDialog: Dialog? = null
    fun showProgressDialog() {
        if (!mIsActivityRunning) return
        hideProgressDialog()
        progressDialog = DialogUtils.progressDialog(this)
        progressDialog!!.show()
    }

    fun hideProgressDialog() {
        if (!mIsActivityRunning) return
        if (progressDialog != null) {
            try {
                progressDialog!!.dismiss()
            } catch (ignored: Exception) {
                ignored.printStackTrace()
            }
        }
    }

    fun finishWithResult(isResultOK: Boolean = true, intent: Intent? = null) {
        val result = if (isResultOK) Activity.RESULT_OK else Activity.RESULT_CANCELED
        setResult(result, intent)
        finish()
    }

    open fun setTopLeftIcon(icon: Drawable?, onClickListener: Consumer<Unit>? = null) {
        showTopLeftIcon(true)
        mAppBarViewModel.mTopLeftIcon.set(icon)
        mAppBarViewModel.mOnTopLeftClickedConsumer = onClickListener
        showTopLeftIcon(true)
    }

    open fun setTopLeftIcon(iconId: Int, onClickListener: Consumer<Unit>? = null) {
        setTopLeftIcon(Utils.getDrawable(iconId), onClickListener)
    }

    fun setTopRightIcon(icon: Drawable?, onClickListener: Consumer<Unit>? = null) {
        mAppBarViewModel.mTopTightIcon.set(icon)
        mAppBarViewModel.mOnTopRightClickedConsumer = onClickListener
        showTopRightIcon(true)
    }

    open fun setTopRightIcon(iconId: Int, onClickListener: Consumer<Unit>? = null) {
        showTopRightIcon(true)
        setTopRightIcon(Utils.getDrawable(iconId), onClickListener)
    }

    fun showTopLeftIcon(isShow: Boolean = true) {
        if (isShow) {
            mAppBarViewModel.mTopLeftVisibility.set(View.VISIBLE)
        } else {
            mAppBarViewModel.mTopLeftVisibility.set(View.GONE)
        }
    }

    fun showTopRightIcon(isShow: Boolean = true) {
        if (isShow) {
            mAppBarViewModel.mTopRightVisibility.set(View.VISIBLE)
        } else {
            mAppBarViewModel.mTopRightVisibility.set(View.GONE)
        }
    }

    fun showCenterIcon(isShow: Boolean = true, title: String? = null) {
        if (isShow) {
            mAppBarViewModel.mCenterIconVisibility.set(View.VISIBLE)
            mAppBarViewModel.mTitleVisibility.set(View.GONE)
        } else {
            mAppBarViewModel.mCenterIconVisibility.set(View.GONE)
            mAppBarViewModel.mTitleVisibility.set(View.VISIBLE)
            mAppBarViewModel.mTitle.set(title)
        }
    }

    fun setNewLocale(languageCode: String?) {
        MainApplication.localeManager?.setNewLocale(this, languageCode)
    }

    fun setUpFullScreenView() {
        mShowFullScreen = true
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }

    fun showFragmentAsContent(fragment: Fragment) {
        mFragmentAsContent = fragment
        supportFragmentManager.beginTransaction().add(android.R.id.content, mFragmentAsContent!!).commit()
    }

    fun removeContentFragment() {
        if (mFragmentAsContent != null) {
            supportFragmentManager.beginTransaction().remove(mFragmentAsContent!!).commit()
            mFragmentAsContent = null
        }
    }

    fun finishWithoutAnimation() {
        finish()
        this.overridePendingTransition(0, android.R.anim.fade_out)
    }
}