package com.vule.toeic365.base

import android.content.Context
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Build
import com.vule.toeic365.utils.SharedPreferenceHelper
import java.util.*

class LocaleManager {

    fun setLocale(c: Context): Context {
        return updateResources(c, getLanguage(c))
    }

    fun setNewLocale(c: Context, language: String?): Context {
        SharedPreferenceHelper.getInstance(c).languageCode = language
        return updateResources(c, language)
    }

    private fun getLanguage(context: Context): String? {
        return SharedPreferenceHelper.getInstance(context).languageCode
    }

    private fun updateResources(context: Context, language: String?): Context {
        var contextVar = context
        val locale = Locale(language)
        Locale.setDefault(locale)
        val res = contextVar.resources
        val config = Configuration(res.configuration)
        config.setLocale(locale)
        contextVar = contextVar.createConfigurationContext(config)
        return contextVar
    }

    fun getLocale(res: Resources): Locale {
        val config = res.configuration
        return if (Build.VERSION.SDK_INT >= 24) config.locales.get(0) else config.locale
    }
}