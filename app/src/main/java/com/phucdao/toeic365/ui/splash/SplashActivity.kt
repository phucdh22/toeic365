package com.vule.toeic365.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.vicpin.krealmextensions.deleteAll
import com.vicpin.krealmextensions.saveAll
import com.vule.toeic365.R
import com.vule.toeic365.base.BaseActivity
import com.vule.toeic365.constant.Constant
import com.vule.toeic365.model.AnswerModel
import com.vule.toeic365.model.PartModel
import com.vule.toeic365.model.PracticeModel
import com.vule.toeic365.model.QuestionModel
import com.vule.toeic365.ui.authorize.AuthorizeActivity
import com.vule.toeic365.ui.home.HomeActivity
import com.vule.toeic365.utils.PreferenceUtils

class SplashActivity : BaseActivity() {

    private val mPartList = arrayListOf(
        PartModel().initData(1,"Part 5 - Beginner"),
        PartModel().initData(2,"Part 5 - Intermediate")
        )
    private val mPracticeList = arrayListOf(
        PracticeModel().initData(1,"Practice 1",1),
        PracticeModel().initData(2,"Practice 2",1),
        PracticeModel().initData(3,"Practice 3",2)
    )
    private val mQuestionList = arrayListOf(
        QuestionModel().apply {
            id = 1
            title = "The manufacture had to……...production due to a lack of raw materials."
            practiceId = 1
            description = "Cuối câu có cụm từ “due to a lack of raw materials (vì thiếu nguyên vật liệu thô)”. Từ đó chúng ta dựa vào nghĩa của đáp án và chọn đáp án cho phù hợp. Vì thiếu nguyên vật liệu nên nên mới hạn chế việc sản xuất. Từ đó, chỉ có câu A. hạn chế là câu đúng.\n" +
                    "Câu B. mở rộng, C. phát triển, D. tăng, đều không hợp nghĩa. "
            translation = ""
        },
        QuestionModel().apply {
            id = 2
            title = "The supervisor was very annoyed…… the new employee who was always late."
            practiceId = 1
            description = "Trong câu này, các bạn cần biết cấu trúc “tobe annoyed with s.b” nghĩa là tức giận ai đó. Ngoài ra còn có cấu trúc “ tobe annoyed at s.t” nghĩa là tức giận về việc gì đó. Trong câu này, đang nói về người “new employee who are always late ( nhân viên mới luôn đi trễ”. Như vậy ta chọn câu D. with"
            translation = ""
        },
        QuestionModel().apply {
            id = 3
            title = "John is really ….. forward to the company vacation this year."
            practiceId = 1
            description = "Trong câu này, các bạn cần biết cấu trúc “look forward to s.t” diễn tả “ trông chờ, mong đợi việc gì đó”."
            translation = ""
        },
        QuestionModel().apply {
            id = 4
            title = "It was when you told me you were arriving on Thursday that my secretary ….. for our driver to pick you up at the airport."
            practiceId = 1
            description = "“It was… that” dùng thì quá khứ, vì thế ta chọn câu B. arranged ( phải là thì quá khứ) để phù hợp với thời. “it was…. That” là mẫu câu dùng để nhấn mạnh cụm từ trước “that”. Câu này được dịch như sau: Khi bạn báo cho tôi biết bạn sẽ đến thứ 5. Thư ký của tôi sẽ sắp xếp tài xế đến sân bây đón bạn. Cấu trúc “ pick s.b up” nghĩa là “đón ai”"
            translation = ""
        },
        QuestionModel().apply {
            id = 5
            title = "We really need those flyers to be ….. by six o’oclock at the latest."
            practiceId = 1
            description = "Từ “flyer” nghĩa là “ tờ rơi quảng cáo”. Tờ rơi quảng cáo là vật. Vì thế “ tờ rơi quảng cáo được phát”. Do đó, ta chọn dạng bị động."
            translation = ""
        },
        QuestionModel().apply {
            id = 6
            title = "The company’s ….. is to buy out smaller competitors, with plans to dominate the northwest region’s party supply center."
            practiceId = 1
            description = "Trong câu có cụm từ “buy out” nghĩa là “ mua lại”. “buy out smaller competitors” nghĩa là “ mua lại công ty đối thủ có quy mô nhỏ. Trong tất cả các đáp án, chỉ có câu A. strategy ( chiến lược) là phù hợp với nghĩa của câu. Các câu còn lại, B. privilege ( đặc quyền), C. domain ( lĩnh vực), D. market ( thị trường), đều không hợp nghĩa."
            translation = ""
        },
        QuestionModel().apply {
            id = 7
            title = "The plane has been….. so that it can land without the help of a pilot."
            practiceId = 1
            description = "Chúng ta thấy “ has been” thì phải thêm “v3” để tạo thành thì hiện tại hoàn thành dạng bị động. Do đó, chọn D là phù hợp nghĩa “ đã được thiết kế xong” Các câu A. demolished ( hủy hoại), B. hurried (vội vàng), C. hung ( treo) đều không phù hợp nghĩa."
            translation = ""
        },
        QuestionModel().apply {
            id = 8
            title = "Have you interviewed any …… applicants for the translator position?"
            practiceId = 1
            description = "“applicants” là danh từ, chỉ “ người xin việc”. Vì thế, chỗ trống chúng ta cần 1 tính từ, đế tính từ bỗ nghĩa cho danh từ “applicants”. Chỉ có câu A. qualified ( đủ tư cách, đủ tiêu chuẩn) là câu trả lời hợp nghĩa. Các câu B. quantified ( xác định số lượng), C. quantity ( số lượng), D. quarterly ( một quí) đều không hợp nghĩa."
            translation = ""
        },
        QuestionModel().apply {
            id = 9
            title = "Jack’s self storage, ….. is located on First and Main streets, is open from seven a.m through seven p.m, seven days a week."
            practiceId = 1
            description = "Nhìn vào câu, chúng ta thấy có dấu “phẩy”. Vì thế, ta loại câu C. ( that không đi chung với dấu phẩy). “jack’s self storage” là vật, cửa hàng của Jack, vì thế ta dùng “which” thay thế cho vật và làm chủ từ trong câu. Sử dụng kiến thức đại từ quan hệ, để làm câu này."
            translation = ""
        },
        QuestionModel().apply {
            id = 10
            title = "We need someone to …. the training program next year."
            practiceId = 1
            description = "Để làm được câu này, các bạn cần biết nghĩa của các cụm từ này. A. Overtake ( vượt qua), B. take over ( tiếp quản), C. hang up ( ngắt điện thoại, cúp máy), D. take out ( lấy ra). Như vậy, chỉ có câu B. là hợp nghĩa với câu hỏi. “ cần một người để tiếp quản chương trình huấn luyện năm tới."
            translation = ""
        },
        QuestionModel().apply {
            id = 11
            title = "In order to have a full lunch hour, we should….. our discussion."
            practiceId = 1
            description = "Để làm được câu này, các bạn cần phải biết nghĩa của các từ trên. A. Undo: trở về ( quay lại vị trí ban đầu, giống như tổ hợp phím “control Z” trong vi tính) B. Unwrap ( phân ra), C. package ( bưu kiện), D. wrap up ( kết thúc) Như vậy, chúng ta thấy chỉ có câu D là hợp nghĩa nhất mà thôi. Để có 1 tiếng nghỉ ăn trưa, chúng ta nên kết thúc cuộc thảo luận"
            translation = ""
        },
        QuestionModel().apply {
            id = 12
            title = "There are plenty of …. available to people who can speak both English and another language fluently."
            practiceId = 1
            description = "Sau cụm từ “Plenty of” ta thêm danh từ số nhiều đếm được hoặc danh từ không đếm được. Trong câu hỏi, chúng ta thấy có chữ “ are”, từ đó, suy ra chỗ trống phải là danh từ đếm được số nhiều. Nghĩa của câu “ có nhiều cơ hội cho người mà có thể nói được ngôn ngữ tiếng anh và ngôn ngữ khác trôi chảy”. Vì thế, ta chọn A. A. Opportunities ( nhiều cơ hội) B. Opportunity ( cơ hội) C. Opportunists ( phần tử đầu cơ) D. Opportunistic ( thuộc về cơ hội, tính từ)"
            translation = ""
        },
        QuestionModel().apply {
            id = 13
            title = "It’s about time something ….. about the office’s broken coffeemaker."
            practiceId = 1
            description = "Để làm được câu này, bạn cần biết cấu trúc “it’s about time (that) + s+ V2 nghĩa là “ là lúc nên làm…. rồi” Chủ ngữ của câu là “something”, như vậy, cái gì được làm, do đó, ta chọn dạng quá khứ đơn dạng bị động, chọn câu B."
            translation = ""
        },
        QuestionModel().apply {
            id = 14
            title = "The newspaper’s account of the incident is based on extensive interviews with more than a dozen of the firm’s managers, some of….. spoke on the condition of anonymity."
            practiceId = 1
            description = "Câu hỏi này khá dài, các bạn cần đọc xung quanh chỗ trống để hiểu ý của câu hỏi. Dịch từ chữ a dozen….. nhé các bạn. Hơn mười mấy người quản lý của công ty, một vài….. phát ngôn giấu tên. Như vậy, chỗ trống này ta chọn B. whom, đại từ quan hệ “whom” thay thế cho người, làm chủ từ trong câu, có thể có giới từ đi theo. Bạn nào không biết làm thì xem lại bài đại từ quan hệ trong tiếng anh nhé! Cấu trúc “ tobe based on” căn cứ vào, dựa vào."
            translation = ""
        },
        QuestionModel().apply {
            id = 15
            title = "The association of realtors have seen a 33% increase in membership over the past seven months with more than 10,349 applications having been processed, …… the facts of the press release are correct."
            practiceId = 1
            description = "Câu này, chúng ta cần dịch sơ để chọn đáp án phù hợp nhé! Dịch từ chỗ 10,349 applications ….. Có hơn 10, 349 hồ sơ được thực hiện, ….. lời nói của phát ngôn là chính xác. Trước chỗ trống là một dấu phẩy, vì thế chúng ta phải điền một liên từ, chỉ có câu C và D là liên từ. Nhưng câu C. though ( mặc dù) không hợp lý về nghĩa. Vì thế, ta chọn câu D. if ( nếu). A. Thereabouts ( khoảng, ở gần đó) B. Somewhat ( phần nào đó, hơi)"
            translation = ""
        },
        QuestionModel().apply {
            id = 16
            title = "Most employees would rather work longer days in ….. more vacation time."
            practiceId = 1
            description = "Để làm được câu này nhanh và chính xác, bạn cần biết cụm từ “ in exchange for” nghĩa là “ đổi lấy”. "
            translation = " Hầu hết nhân viên muốn làm việc dài ngày hơn để đổi lấy kỳ nghỉ lâu hơn."
        },
        QuestionModel().apply {
            id = 17
            title = "The capital raised from the initial public offering will be used to…. the company’s mining exploration plans."
            practiceId = 1
            description = "“be used to” nghĩa là “ được dùng để làm việc gì đó”, sau nó ta dùng động từ dạng nguyên mẫu nhé! Vì thế, chọn câu D finance ( cứu trợ, cấp tiền) nhé! Một số từ vựng cần biết Capital raised: số tiền được quyên góp. Số tiền được huy động. Initial public offering: lần đầu phát hành công khai Exploration: khai thác"
            translation = ""
        },
        QuestionModel().apply {
            id = 18
            title = "The board of directors…..against a dividend payout to investors this quarter."
            practiceId = 1
            description = "Câu này chưa có động từ, chỗ trống cần một động từ. chỉ có câu B và D là động từ. Ta chọn câu B. decided ( quyết định), hợp nghĩa với câu hỏi. Câu A. decision ( sự quyết định-danh từ), C. decisive ( thuốc về quyết đinh- tính từ), D. decreed ( phân bổ-động từ) Một số từ vựng. Decided against: quyết định phản đối Dividend payout: chia cổ tức Investor: nhà đầu tư"
            translation = ""
        },
        QuestionModel().apply {
            id = 19
            title = "A diversified portfolio will….. the risk involved with investing large sums of money in the stock exchange."
            practiceId = 1
            description = "Sau will, chúng ta thêm động từ dạng nguyên mẫu, nhìn vào đáp án, ta thấy câu A. minimaize (hạn chế tối đa) là động từ (thường có chữ “ze” cuối sẽ là động từ). Các câu còn lại không phú hợp. B. rất nhỏ, tối thiểu- tính từ, C. minimum ( ít nhất, là danh từ), D. minute ( phút, danh từ). Diversified portfolio : danh mục đầu tư đa dạng hóa. Stock exchange: chứng khoán, thị trường cổ phiếu."
            translation = "Danh mục đầu tư đa dạng hóa sẽ giảm thiểu rủi ro một số tiền lớn trong việc đầu tư vào thị trường cổ phiếu."
        },
        QuestionModel().apply {
            id = 20
            title = "Dr. Vince’s arguments on the future of the chemical industry were…. enough that funding for his current project was doubled."
            practiceId = 1
            description = "Sau “were” có thể là danh từ, tính từ hoặc dạng động từ thêm ing. Tuy nhiên trong trường hợp này, chúng ta không chọn dạng ving, vì không có hành động gì đang tiếp diễn cả. Nhìn vào đáp án, ta chỉ thấy có câu A và B là thỏa mãn, vì đều là tính từ. Ta chọn câu A. mang nghĩa chủ động, còn câu B mang nghĩa bị động ( bị thuyết phục) không hợp lý về nghĩa. “Enough” đứng ở 2 vị trí sau: Enough đứng trước danh từ: Ex: enough money"
            translation = ""
        },
        QuestionModel().apply {
            id = 21
            title = "I need to get the proposal back which I …….. you last week. "
            practiceId = 2
            description = "Dựa vào nghĩa, “ tôi muốn nhận lại đề xuất mà tôi…….cho bạn tuần rồi”. Như vậy, đề xuất đã được gửi đi thì mới muốn nhận lại. ta chọn câu A.  "
            translation = ""
        },
        QuestionModel().apply {
            id = 22
            title = "My boss …… by the general manager.  "
            practiceId = 2
            description = "Câu này đang thiếu động từ, lại có chữ by + danh từ, nên suy ra là câu bị động. Vì thế, ta chọn câu A. intimidate (hăm dọa) "
            translation = "Dịch: Sếp của tôi rất sợ tổng giám đốc "
        },
        QuestionModel().apply {
            id = 23
            title = "…….. our decreased revenue, we have been forced to reduce expenses. "
            practiceId = 2
            description = "Dựa vào nghĩa của câu, ……. thu nhập của chúng tôi giảm, chúng tôi cắt giảm chi tiêu.  Vì thế, ta chọn C. due to ( vì, do). "
            translation = ""
        },
        QuestionModel().apply {
            id = 24
            title = "Our new benefits package seems to be quite popular …… our employees. "
            practiceId = 2
            description = "Cấu trúc “be popular with” nghĩa là ưa chuộng, phổ biến. Vì thế, ta chọn câu B. "
            translation = ""
        },
        QuestionModel().apply {
            id = 25
            title = " ……… what happens, I’ll have my report on your desk by-Friday morning.  "
            practiceId = 2
            description = "No matter what= whatever : dù bất cứ gì đi nữa, dù có chuyện gì đi nữa. "
            translation = "Dịch: dù có chuyện gì xảy ra đi nữa, tôi cũng sẽ để bài báo cáo của tôi trên bàn của ông trước sáng thứ 6. "
        },
        QuestionModel().apply {
            id = 26
            title = "Employees were allowed to invite three ….. to the company picnic, such as their spouse or other family members. "
            practiceId = 2
            description = "Câu này ta dịch nghĩa để chọn câu trả lời phù hợp. Nhân viên được phép mời 3 ….. tham gia cuộc dã ngoại như vợ chồng hoặc các thành viên trong gia đình. Như vậy, chỉ có câu A. guest ( khách) là hợp lý. "
            translation = ""
        },
        QuestionModel().apply {
            id = 27
            title = "Technology has made it possible for objects to travel …… the speed of light. "
            practiceId = 2
            description = "Dựa vào nghĩa “ công nghệ kỹ thuật  đã làm tốc độ của vật thể …. tốc độ của ánh sáng” \n" +
                    "Như vây, chỉ có thể là câu A. nhanh hơn.  \n" +
                    "Câu B. the same as không thể hình dung tốc độ, phải sửa lại là as fast as.  \n" +
                    "Câu C: bình thường tốc độ của vật thể đã chậm hơn so với tốc độ của ánh sáng rồi ( đâu cần nhờ vào công nghệ nữa) nên không hợp lý.  \n" +
                    "Câu D. more quickly phải thêm “ than” mới đúng ngữ pháp ( so sánh hơn). "
            translation = ""
        },
        QuestionModel().apply {
            id = 28
            title = "It looks like you could use some help ……. for the conference. "
            practiceId = 2
            description = "Câu gốc trong trường hợp này là “ some help in preparing”. Tuy nhiên, chúng ta có thể lượt giản giới từ “in” còn lài là “some help preparing”( cần sự giúp đỡ về ….). Vì thế, ta chọn D."
            translation = ""
        },
        QuestionModel().apply {
            id = 29
            title = "Contrary to popular belief, significant revenue does not always ….. into profit. "
            practiceId = 2
            description = "Dựa vào nghĩa của câu “ ….doanh thu đáng kể không phải lúc nào cũng …….với lợi nhuận” .  \n" +
                    "Chỗ trống chúng ta thêm “translate” nghĩa là bằng. vì thế, chọn đáp án C. "
            translation = ""
        },
        QuestionModel().apply {
            id = 30
            title = "The first thing most employers look for in an employee is a good …… "
            practiceId = 2
            description = "Dựa vào nghĩa “ điều đầu tiên hầu hết các ông chủ mong muốn ở nhân viên là ….. tốt.  \n" +
                    "Chỗ trống chỉ có câu C. attitude ( thái độ) là hợp lý."
            translation = ""
        },
        QuestionModel().apply {
            id = 31
            title = "It is difficult to be successful in business without some kind of …….  Advantage."
            practiceId = 3
            description = "Advantage là danh từ. Vì thế, chỗ trống chúng ta cần điền tính từ. Vì thế ta chọn câu C. "
            translation = "Dịch: khó khăn để thành công trong việc kinh doanh mà không có một vài lợi thế cạnh tranh"
        },
        QuestionModel().apply {
            id = 32
            title = "It is impossible to maintain a …….. relationship with someone without trust. "
            practiceId = 3
            description = "Dựa vào nghĩa của câu “ không thể duy trì …… mối quan hệ với người không đáng tin cậy”. Như vậy ta chọn câu A. công việc ( mối quan hệ công việc) "
            translation = ""
        },
        QuestionModel().apply {
            id = 33
            title = "Each manager formatted their report differently, so jack was asked to make the report look …… "
            practiceId = 3
            description = "Dựa vào nghĩa của câu “ mỗi người quản lý định dạng bài báo cáo của họ khác nhau, do đó Jack được yêu cầu làm bài báo cáo giống ….” .  \n" +
                    "A. đồng nghĩa, B. đơn điệu, C. đồng thời, D. đồng nhất. như vậy ta chọn câu D. “đồng nhất”  là phù hợp với nghĩa của câu.  "
            translation = ""
        },
        QuestionModel().apply {
            id = 34
            title = " John said he couldn’t make the retreat because he had ……. made plans for the weekend. "
            practiceId = 3
            description = "Câu này là thí quá khứ hoàn thành ( diễn tả một hành động xảy ra trước một hành động khác ở quá khứ). “already” thường đi với thì hoàn thành ở dạng khẳng đinh. Vì thế, ta chọn câu C. "
            translation = "Anh ta nói anh ta không thể tham gia trại cứu tế bởi vì anh ta đã có kế hoạch vào các ngày cuối tuần rồi. "
        },
        QuestionModel().apply {
            id = 35
            title = "Your presentation is excellent, but you might want to …… it by a couple minutes. "
            practiceId = 3
            description = "Dựa vào nghĩa của câu “ bài thuyết trình của bạn tuyệt vời nhưng có lẻ bạn muốn …… thời gian vài phút”. Sau “want to” ta thêm động từ nguyên mẫu. Vì thế ta chọn câu B. "
            translation = ""
        },
        QuestionModel().apply {
            id = 36
            title = "I expect everyone to ……… the company banquet next Saturday night. "
            practiceId = 3
            description = "Câu này, ta dịch xung quanh chỗ trống “ I expect everyone to….. the company banquet”. Tôi hy vọng mọi người…… bữa tiệc lớn của công ty. Vì thế ta chọn câu A. attend ( tham gia, tham dự).  "
            translation = ""
        },
        QuestionModel().apply {
            id = 37
            title = "There will be a \$10.000 bonus to staff member with the ……. sales this month.  "
            practiceId = 3
            description = "Khi nói về “sales” việc bán hàng thì ta chỉ có thể chọ câu D. highest (cao), để tạo thành cụm từ “Doanh số bán hàng cao”. "
            translation = ""
        },
        QuestionModel().apply {
            id = 38
            title = "There are both advantages and disadvantages ……. driving a car to work.  "
            practiceId = 3
            description = "Advantage hoặc disadvantage đều đi với giới từ “of”. Vì thế ta chọn C.  "
            translation = "Có những thuận lợi và khó khăn trong việc lái xe đi làm.  "
        },
        QuestionModel().apply {
            id = 39
            title = "You will be provided …… an extensive handbook at the start of the seminar.  "
            practiceId = 3
            description = "Provide somebody with something hoặc provide something for somebody nghĩa là cung cấp cái gì cho ai. Câu này là câu bị động, chủ từ là người, nên dùng giới từ “with” cộng với sự vật phía sau. Ta chọn B. "
            translation = ""
        },
        QuestionModel().apply {
            id = 40
            title = "Someone is going to have to ….  the blame for losing that file. "
            practiceId = 3
            description = "Cụm từ “ take the blame for something” chịu trách nhiệm về việc gì đó. Do dó, ta chọn A.  "
            translation = ""
        }
    )
    private val mAnswerList = arrayListOf(
        AnswerModel().apply {
            id = 1
            title = "A. Limit "
            isCorrect = true
            questionId = 1
        },
        AnswerModel().apply {
            id = 2
            title = "B. Expand"
            isCorrect = false
            questionId = 1
        },
        AnswerModel().apply {
            id = 3
            title = "C. Develop "
            isCorrect = false
            questionId = 1
        },
        AnswerModel().apply {
            id = 4
            title = "D. Increase"
            isCorrect = false
            questionId = 1
        },
        AnswerModel().apply {
            id = 5
            title = "A. For"
            isCorrect = false
            questionId = 2
        },
        AnswerModel().apply {
            id = 6
            title = "B. To "
            isCorrect = false
            questionId = 2
        },
        AnswerModel().apply {
            id = 7
            title = "C. In "
            isCorrect = false
            questionId = 2
        },
        AnswerModel().apply {
            id = 8
            title = "D. With "
            isCorrect = true
            questionId = 2
        },
        AnswerModel().apply {
            id = 9
            title = "A Going"
            isCorrect = false
            questionId = 3
        },
        AnswerModel().apply {
            id = 10
            title = "B. Seeing"
            isCorrect = false
            questionId = 3
        },
        AnswerModel().apply {
            id = 11
            title = "C. Looking "
            isCorrect = true
            questionId = 3
        },
        AnswerModel().apply {
            id = 12
            title = "D. Moving"
            isCorrect = false
            questionId = 3
        },
        AnswerModel().apply {
            id = 13
            title = "A. Arranges"
            isCorrect = false
            questionId = 4
        },
        AnswerModel().apply {
            id = 14
            title = "B. Arranged "
            isCorrect = true
            questionId = 4
        },
        AnswerModel().apply {
            id = 15
            title = "C. Arranging"
            isCorrect = false
            questionId = 4
        },
        AnswerModel().apply {
            id = 16
            title = "D. Arrangement"
            isCorrect = false
            questionId = 4
        },
        AnswerModel().apply {
            id = 17
            title = "A. Deliveries"
            isCorrect = false
            questionId = 5
        },
        AnswerModel().apply {
            id = 18
            title = "B. Delivering "
            isCorrect = false
            questionId = 5
        },
        AnswerModel().apply {
            id = 19
            title = "C. Deliver"
            isCorrect = false
            questionId = 5
        },
        AnswerModel().apply {
            id = 20
            title = "D. Delivered"
            isCorrect = true
            questionId = 5
        },
        AnswerModel().apply {
            id = 21
            title = "A. Strategy"
            isCorrect = true
            questionId = 6
        },
        AnswerModel().apply {
            id = 22
            title = "B. Privilege "
            isCorrect = false
            questionId = 6
        },
        AnswerModel().apply {
            id = 23
            title = "C. Domain"
            isCorrect = false
            questionId = 6
        },
        AnswerModel().apply {
            id = 24
            title = "D. Market"
            isCorrect = false
            questionId = 6
        },
        AnswerModel().apply {
            id = 25
            title = "A. Demolished "
            isCorrect = false
            questionId = 7
        },
        AnswerModel().apply {
            id = 26
            title = "B. Hurried "
            isCorrect = false
            questionId = 7
        },
        AnswerModel().apply {
            id = 27
            title = "C. Hung"
            isCorrect = false
            questionId = 7
        },
        AnswerModel().apply {
            id = 28
            title = "D. Designed"
            isCorrect = true
            questionId = 7
        },
        AnswerModel().apply {
            id = 29
            title = "A. Qualified "
            isCorrect = true
            questionId = 8
        },
        AnswerModel().apply {
            id = 30
            title = "B. Quantified "
            isCorrect = false
            questionId = 8
        },
        AnswerModel().apply {
            id = 31
            title = "C. Quantity "
            isCorrect = false
            questionId = 8
        },
        AnswerModel().apply {
            id = 32
            title = "D. Quarterly"
            isCorrect = false
            questionId = 8
        },
        AnswerModel().apply {
            id = 33
            title = "A. Which"
            isCorrect = true
            questionId = 9
        },
        AnswerModel().apply {
            id = 34
            title = "B. There "
            isCorrect = false
            questionId = 9
        },
        AnswerModel().apply {
            id = 35
            title = "C. That "
            isCorrect = false
            questionId = 9
        },
        AnswerModel().apply {
            id = 36
            title = "D. Where"
            isCorrect = false
            questionId = 9
        },
        AnswerModel().apply {
            id = 37
            title = "A. Overtake "
            isCorrect = false
            questionId = 10
        },
        AnswerModel().apply {
            id = 38
            title = "B. Take over "
            isCorrect = true
            questionId = 10
        },
        AnswerModel().apply {
            id = 39
            title = "C. Hang up"
            isCorrect = false
            questionId = 10
        },
        AnswerModel().apply {
            id = 40
            title = "D. Take out"
            isCorrect = false
            questionId = 10
        },
        AnswerModel().apply {
            id = 41
            title = "A. Undo"
            isCorrect = false
            questionId = 11
        },
        AnswerModel().apply {
            id = 42
            title = "B. Unwrap"
            isCorrect = false
            questionId = 11
        },
        AnswerModel().apply {
            id = 43
            title = "C. Package "
            isCorrect = false
            questionId = 11
        },
        AnswerModel().apply {
            id = 44
            title = "D. Wrap up"
            isCorrect = true
            questionId = 11
        },
        AnswerModel().apply {
            id = 45
            title = "A. Opportunities "
            isCorrect = true
            questionId = 12
        },
        AnswerModel().apply {
            id = 46
            title = "B. Opportunity "
            isCorrect = false
            questionId = 12
        },
        AnswerModel().apply {
            id = 47
            title = "C. Opportunists "
            isCorrect = false
            questionId = 12
        },
        AnswerModel().apply {
            id = 48
            title = "D. Opportunistic"
            isCorrect = false
            questionId = 12
        },
        AnswerModel().apply {
            id = 49
            title = "A. Had better "
            isCorrect = false
            questionId = 13
        },
        AnswerModel().apply {
            id = 50
            title = "B. Was done "
            isCorrect = true
            questionId = 13
        },
        AnswerModel().apply {
            id = 51
            title = " C. Is doing"
            isCorrect = false
            questionId = 13
        },
        AnswerModel().apply {
            id = 52
            title = "D. Has been"
            isCorrect = false
            questionId = 13
        },
        AnswerModel().apply {
            id = 53
            title = "A. Which "
            isCorrect = false
            questionId = 14
        },
        AnswerModel().apply {
            id = 54
            title = "B. Whom "
            isCorrect = true
            questionId = 14
        },
        AnswerModel().apply {
            id = 55
            title = "C. Them"
            isCorrect = false
            questionId = 14
        },
        AnswerModel().apply {
            id = 56
            title = "D. Whose"
            isCorrect = false
            questionId = 14
        },
        AnswerModel().apply {
            id = 57
            title = "A. Thereabouts"
            isCorrect = false
            questionId = 15
        },
        AnswerModel().apply {
            id = 58
            title = "B. Somewhat"
            isCorrect = false
            questionId = 15
        },
        AnswerModel().apply {
            id = 59
            title = "C. Though "
            isCorrect = false
            questionId = 15
        },
        AnswerModel().apply {
            id = 60
            title = "D. If"
            isCorrect = true
            questionId = 15
        },
        AnswerModel().apply {
            id = 61
            title = "A. Exchange for"
            isCorrect = true
            questionId = 16
        },
        AnswerModel().apply {
            id = 62
            title = "B. Change to"
            isCorrect = false
            questionId = 16
        },
        AnswerModel().apply {
            id = 63
            title = "C. Changing"
            isCorrect = false
            questionId = 16
        },
        AnswerModel().apply {
            id = 64
            title = "D. Exchange to"
            isCorrect = false
            questionId = 16
        },
        AnswerModel().apply {
            id = 65
            title = "A. Financed "
            isCorrect = false
            questionId = 17
        },
        AnswerModel().apply {
            id = 66
            title = "B. Financial "
            isCorrect = false
            questionId = 17
        },
        AnswerModel().apply {
            id = 67
            title = "C. Financing "
            isCorrect = false
            questionId = 17
        },
        AnswerModel().apply {
            id = 68
            title = "D. Finance"
            isCorrect = true
            questionId = 17
        },
        AnswerModel().apply {
            id = 69
            title = "A. Decision"
            isCorrect = false
            questionId = 18
        },
        AnswerModel().apply {
            id = 70
            title = " B. Decided"
            isCorrect = true
            questionId = 18
        },
        AnswerModel().apply {
            id = 71
            title = " C. Decisive"
            isCorrect = false
            questionId = 18
        },
        AnswerModel().apply {
            id = 72
            title = " D. Decreed"
            isCorrect = false
            questionId = 18
        },
        AnswerModel().apply {
            id = 73
            title = "A. Minimize "
            isCorrect = true
            questionId = 19
        },
        AnswerModel().apply {
            id = 74
            title = "B. Minimal "
            isCorrect = false
            questionId = 19
        },
        AnswerModel().apply {
            id = 75
            title = "C. Minimum"
            isCorrect = false
            questionId = 19
        },
        AnswerModel().apply {
            id = 76
            title = "D. Minute"
            isCorrect = false
            questionId = 19
        },
        AnswerModel().apply {
            id = 77
            title = "A. Convincing "
            isCorrect = true
            questionId = 20
        },
        AnswerModel().apply {
            id = 78
            title = "B. Convinced "
            isCorrect = false
            questionId = 20
        },
        AnswerModel().apply {
            id = 79
            title = "C. Convince "
            isCorrect = false
            questionId = 20
        },
        AnswerModel().apply {
            id = 80
            title = "D. Convincingly"
            isCorrect = false
            questionId = 20
        },
        AnswerModel().apply {
            id = 81
            title = "A. sent "
            isCorrect = true
            questionId = 21
        },
        AnswerModel().apply {
            id = 82
            title = "B. borrowed"
            isCorrect = false
            questionId = 21
        },
        AnswerModel().apply {
            id = 83
            title = "C. wrote "
            isCorrect = false
            questionId = 21
        },
        AnswerModel().apply {
            id = 84
            title = "D. read "
            isCorrect = false
            questionId = 21
        },
        AnswerModel().apply {
            id = 85
            title = "A. is intimidated "
            isCorrect = true
            questionId = 22
        },
        AnswerModel().apply {
            id = 86
            title = "B. has intimidated "
            isCorrect = false
            questionId = 22
        },
        AnswerModel().apply {
            id = 87
            title = "C. is intimidating "
            isCorrect = false
            questionId = 22
        },
        AnswerModel().apply {
            id = 88
            title = "D. intimidates "
            isCorrect = false
            questionId = 22
        },
        AnswerModel().apply {
            id = 89
            title = "A. regardless of "
            isCorrect = false
            questionId = 23
        },
        AnswerModel().apply {
            id = 90
            title = "B. depending on  "
            isCorrect = false
            questionId = 23
        },
        AnswerModel().apply {
            id = 91
            title = "C. due to "
            isCorrect = true
            questionId = 23
        },
        AnswerModel().apply {
            id = 92
            title = "D. according to "
            isCorrect = false
            questionId = 23
        },
        AnswerModel().apply {
            id = 93
            title = "A. for "
            isCorrect = false
            questionId = 24
        },
        AnswerModel().apply {
            id = 94
            title = "B. with "
            isCorrect = true
            questionId = 24
        },
        AnswerModel().apply {
            id = 95
            title = "C. about "
            isCorrect = false
            questionId = 24
        },
        AnswerModel().apply {
            id = 96
            title = "D. from "
            isCorrect = false
            questionId = 24
        },
        AnswerModel().apply {
            id = 97
            title = "A. however "
            isCorrect = false
            questionId = 25
        },
        AnswerModel().apply {
            id = 98
            title = "B. no matter "
            isCorrect = true
            questionId = 25
        },
        AnswerModel().apply {
            id = 99
            title = "C. apart from  "
            isCorrect = false
            questionId = 25
        },
        AnswerModel().apply {
            id = 100
            title = "D. despite "
            isCorrect = false
            questionId = 25
        },
        AnswerModel().apply {
            id = 101
            title = "A. guests "
            isCorrect = true
            questionId = 26
        },
        AnswerModel().apply {
            id = 102
            title = "B. clients "
            isCorrect = false
            questionId = 26
        },
        AnswerModel().apply {
            id = 103
            title = "C. visitors "
            isCorrect = false
            questionId = 26
        },
        AnswerModel().apply {
            id = 104
            title = "D. customers "
            isCorrect = false
            questionId = 26
        },
        AnswerModel().apply {
            id = 105
            title = "A. faster than "
            isCorrect = true
            questionId = 27
        },
        AnswerModel().apply {
            id = 106
            title = "B. the same as "
            isCorrect = false
            questionId = 27
        },
        AnswerModel().apply {
            id = 107
            title = "C. slower than "
            isCorrect = false
            questionId = 27
        },
        AnswerModel().apply {
            id = 108
            title = "D. more quickly "
            isCorrect = false
            questionId = 27
        },
        AnswerModel().apply {
            id = 109
            title = "A. prepare "
            isCorrect = false
            questionId = 28
        },
        AnswerModel().apply {
            id = 110
            title = "B. prepared "
            isCorrect = false
            questionId = 28
        },
        AnswerModel().apply {
            id = 111
            title = "C. preparation "
            isCorrect = false
            questionId = 28
        },
        AnswerModel().apply {
            id = 112
            title = "D. preparing "
            isCorrect = true
            questionId = 28
        },
        AnswerModel().apply {
            id = 113
            title = "A. transcend "
            isCorrect = false
            questionId = 29
        },
        AnswerModel().apply {
            id = 114
            title = "B. transport "
            isCorrect = false
            questionId = 29
        },
        AnswerModel().apply {
            id = 115
            title = "C. translate "
            isCorrect = true
            questionId = 29
        },
        AnswerModel().apply {
            id = 116
            title = "D. transgress "
            isCorrect = false
            questionId = 29
        },
        AnswerModel().apply {
            id = 117
            title = "A. standing "
            isCorrect = false
            questionId = 30
        },
        AnswerModel().apply {
            id = 118
            title = "B. appearance "
            isCorrect = false
            questionId = 30
        },
        AnswerModel().apply {
            id = 119
            title = "C. attitude  "
            isCorrect = true
            questionId = 30
        },
        AnswerModel().apply {
            id = 120
            title = "D. occupation "
            isCorrect = false
            questionId = 30
        },
        AnswerModel().apply {
            id = 121
            title = "A. competing "
            isCorrect = false
            questionId = 31
        },
        AnswerModel().apply {
            id = 122
            title = "B. competition "
            isCorrect = false
            questionId = 31
        },
        AnswerModel().apply {
            id = 123
            title = "C. competitive "
            isCorrect = true
            questionId = 31
        },
        AnswerModel().apply {
            id = 124
            title = "D. competitively "
            isCorrect = false
            questionId = 31
        },
        AnswerModel().apply {
            id = 125
            title = "A. working "
            isCorrect = true
            questionId = 32
        },
        AnswerModel().apply {
            id = 126
            title = "B. wanting "
            isCorrect = false
            questionId = 32
        },
        AnswerModel().apply {
            id = 127
            title = "C. waning "
            isCorrect = false
            questionId = 32
        },
        AnswerModel().apply {
            id = 128
            title = "D. willing "
            isCorrect = false
            questionId = 32
        },
        AnswerModel().apply {
            id = 129
            title = "A. synonymous "
            isCorrect = false
            questionId = 33
        },
        AnswerModel().apply {
            id = 130
            title = "B. monotonous  "
            isCorrect = false
            questionId = 33
        },
        AnswerModel().apply {
            id = 131
            title = "C. simultaneous "
            isCorrect = false
            questionId = 33
        },
        AnswerModel().apply {
            id = 132
            title = "D. uniform "
            isCorrect = true
            questionId = 33
        },
        AnswerModel().apply {
            id = 133
            title = "A. never "
            isCorrect = false
            questionId = 34
        },
        AnswerModel().apply {
            id = 134
            title = "B. since "
            isCorrect = false
            questionId = 34
        },
        AnswerModel().apply {
            id = 135
            title = "C. already "
            isCorrect = true
            questionId = 34
        },
        AnswerModel().apply {
            id = 136
            title = "D. anyway "
            isCorrect = false
            questionId = 34
        },
        AnswerModel().apply {
            id = 137
            title = "A. be shortened  "
            isCorrect = false
            questionId = 35
        },
        AnswerModel().apply {
            id = 138
            title = "B. shorten  "
            isCorrect = true
            questionId = 35
        },
        AnswerModel().apply {
            id = 139
            title = "C. be shorten  "
            isCorrect = false
            questionId = 35
        },
        AnswerModel().apply {
            id = 140
            title = "D. have shortened "
            isCorrect = false
            questionId = 35
        },
        AnswerModel().apply {
            id = 141
            title = "A. attend "
            isCorrect = true
            questionId = 36
        },
        AnswerModel().apply {
            id = 142
            title = "B. apply "
            isCorrect = false
            questionId = 36
        },
        AnswerModel().apply {
            id = 143
            title = "C. adopt "
            isCorrect = false
            questionId = 36
        },
        AnswerModel().apply {
            id = 144
            title = "D. adapt "
            isCorrect = false
            questionId = 36
        },
        AnswerModel().apply {
            id = 145
            title = "A. furthest "
            isCorrect = false
            questionId = 37
        },
        AnswerModel().apply {
            id = 146
            title = "B. farthest "
            isCorrect = false
            questionId = 37
        },
        AnswerModel().apply {
            id = 147
            title = "C. tallest "
            isCorrect = false
            questionId = 37
        },
        AnswerModel().apply {
            id = 148
            title = "D. highest "
            isCorrect = true
            questionId = 37
        },
        AnswerModel().apply {
            id = 149
            title = "A. with "
            isCorrect = false
            questionId = 38
        },
        AnswerModel().apply {
            id = 150
            title = "B. on "
            isCorrect = false
            questionId = 38
        },
        AnswerModel().apply {
            id = 151
            title = "C. of "
            isCorrect = true
            questionId = 38
        },
        AnswerModel().apply {
            id = 152
            title = "D. in "
            isCorrect = false
            questionId = 38
        },
        AnswerModel().apply {
            id = 153
            title = "A. for "
            isCorrect = false
            questionId = 39
        },
        AnswerModel().apply {
            id = 154
            title = "B. with "
            isCorrect = true
            questionId = 39
        },
        AnswerModel().apply {
            id = 155
            title = "C. against "
            isCorrect = false
            questionId = 39
        },
        AnswerModel().apply {
            id = 156
            title = "D. to "
            isCorrect = false
            questionId = 39
        },
        AnswerModel().apply {
            id = 157
            title = "A. take "
            isCorrect = true
            questionId = 40
        },
        AnswerModel().apply {
            id = 158
            title = "B. make "
            isCorrect = false
            questionId = 40
        },
        AnswerModel().apply {
            id = 159
            title = "C. have "
            isCorrect = false
            questionId = 40
        },
        AnswerModel().apply {
            id = 160
            title = "D. find "
            isCorrect = false
            questionId = 40
        }
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        saveDummyData()
        val currentUserEmail = PreferenceUtils.getFromPrefs(this,Constant.CURRENT_USER_EMAIL_KEY,"") as? String
        if(!currentUserEmail.isNullOrEmpty()){
            startActivity(Intent(this,HomeActivity::class.java))
        } else {
            startActivity(Intent(this,AuthorizeActivity::class.java))
        }
        finish()
    }

    private fun saveDummyData(){
        PartModel().deleteAll()
        PracticeModel().deleteAll()
        QuestionModel().deleteAll()
        AnswerModel().deleteAll()
        //
        mPartList.saveAll()
        mPracticeList.saveAll()
        mQuestionList.saveAll()
        mAnswerList.saveAll()
    }
}
