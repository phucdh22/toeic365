package com.vule.toeic365.ui.subfunction

import android.os.CountDownTimer
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.vicpin.krealmextensions.query
import com.vicpin.krealmextensions.save
import com.vule.toeic365.BR
import com.vule.toeic365.R
import com.vule.toeic365.adapter.PracticeDetailAdapter
import com.vule.toeic365.adapter.SubmitPracticeAdapter
import com.vule.toeic365.base.BaseBindingModelActivity
import com.vule.toeic365.constant.Constant
import com.vule.toeic365.databinding.ActivityFunctionBinding
import com.vule.toeic365.listener.PracticeDetailListener
import com.vule.toeic365.listener.PracticeSubmitListener
import com.vule.toeic365.model.AnswerModel
import com.vule.toeic365.model.QuestionModel
import com.vule.toeic365.model.SubmitModel
import com.vule.toeic365.model.UserAnswerModel
import com.vule.toeic365.utils.DialogUtils
import com.vule.toeic365.utils.PreferenceUtils
import io.reactivex.functions.Consumer
import io.realm.RealmList
import java.util.*
import kotlin.collections.ArrayList

class FunctionActivity : BaseBindingModelActivity<ActivityFunctionBinding,FunctionViewModel>(),
    PracticeDetailListener, PracticeSubmitListener {

    private lateinit var mPracticeDetailAdapter: PracticeDetailAdapter
    private lateinit var mSubmitPracticeAdapter: SubmitPracticeAdapter
    private lateinit var mQuestionList: ArrayList<QuestionModel>
    private var isReviewing = false
    private var mCurrentUserEmail: String = ""
    private var mTimeField = "00:00"
    private lateinit var mTimerCountDown: CountDownTimer

    override fun onUserAnswer(position: Int,questionId: Int, answerId: Int) {
        val userAnswerModel = UserAnswerModel().apply {
            this.userEmail = mCurrentUserEmail
            this.partId = PreferenceUtils.getFromPrefs(this@FunctionActivity,Constant.CURRENT_PART_ID,1) as Int
            this.practiceId = PreferenceUtils.getFromPrefs(this@FunctionActivity,Constant.CURRENT_PRACTICE_ID,1) as Int
            this.questionId = questionId
            this.answerId = answerId
        }
        userAnswerModel.save()
        mQuestionList[position].isSelected = true
        mPracticeDetailAdapter.notifyItemChanged(position)
        mSubmitPracticeAdapter.notifyItemChanged(position)
    }

    override fun onQuestionClicked(position: Int) {
        mViewModel.mIsSubmitVisible.set(false)
        mLayoutBinding.vpPracticeDetail.currentItem = position
    }

    override fun layoutId(): Int = R.layout.activity_function
    override fun viewModelClass(): Class<FunctionViewModel> = FunctionViewModel::class.java
    override fun bindingVariable(): Int = BR.viewModel

    override fun setupView() {
        super.setupView()
        getCurrentUser()
        checkReviewing()
        initPracticeViewPager()
        initSubmitRecyclerView()
        subscribeListener()
        if(isReviewing){
            mLayoutBinding.btnSubmit.visibility = View.GONE
        } else {
            var currentTime = 0
            mTimerCountDown = object : CountDownTimer(120000,1000){
                override fun onFinish() {
                    submitPractice()
                }

                override fun onTick(millisUntilFinished: Long) {
                    mTimeField = ""
                    currentTime += 1000
                    val time = currentTime/1000++
                    val timeInMinute = time/60
                    val timeInSecond = time%60
                    mTimeField += if(timeInMinute < 10) "0$timeInMinute" else "$timeInMinute"
                    mTimeField += if(timeInSecond < 10) ":0$timeInSecond" else ":$timeInSecond"
                    mLayoutBinding.tvTime.text = mTimeField
                }
            }
            mTimerCountDown.start()
        }

    }

    private fun checkReviewing(){
        val practiceId = PreferenceUtils.getFromPrefs(this,Constant.CURRENT_PRACTICE_ID,1) as Int
        val submittedPractice = SubmitModel().query { equalTo(Constant.mPracticeIdField,practiceId) }.lastOrNull { it.userEmail ==  mCurrentUserEmail}
        isReviewing = submittedPractice != null
        //
        mQuestionList = QuestionModel().query { equalTo(Constant.mPracticeIdField,practiceId) } as ArrayList
        if(isReviewing){
            submittedPractice?.selectedQuestionId?.forEach {questionId ->
                for(i in 0 until mQuestionList.size){
                    val question = mQuestionList[i]
                    if(questionId == question.id){
                        mQuestionList[i].isSelected = true
                    }
                }
            }
        }
    }

    private fun initSubmitRecyclerView(){
        mSubmitPracticeAdapter = SubmitPracticeAdapter(mQuestionList,isReviewing,this)
        mLayoutBinding.rvSubmit.layoutManager = GridLayoutManager(this,5)
        mLayoutBinding.rvSubmit.adapter = mSubmitPracticeAdapter
    }


    private fun initPracticeViewPager(){

        mPracticeDetailAdapter = PracticeDetailAdapter(mQuestionList,isReviewing,this,mCurrentUserEmail)
        mLayoutBinding.vpPracticeDetail.adapter = mPracticeDetailAdapter
    }

    private fun subscribeListener(){
        mViewModel.mShowToastConsumer = Consumer { message ->
            DialogUtils.showToastDialog(this,message)
        }
        mViewModel.mShowConfirmDialog = Consumer {
            if(isReviewing){
                this.finish()
            } else {
                DialogUtils.showConfirmDialog(this,Consumer{
                    this.finish()
                })
            }
        }
        mLayoutBinding.btnSubmit.setOnClickListener {
            submitPractice()
        }
    }

    private fun submitPractice(){
        mTimerCountDown.cancel()
        val practiceId = PreferenceUtils.getFromPrefs(this,Constant.CURRENT_PRACTICE_ID,1) as Int
        val score = getScore()
        val questionIds = getSelectedQuestion()
        val dateString = getCompleteDate()
        val submitModel = SubmitModel().apply {
            this.id = mCurrentUserEmail+practiceId
            this.practiceId = practiceId
            this.userEmail = mCurrentUserEmail
            this.score = score
            this.time = mTimeField
            this.selectedQuestionId = questionIds
            this.completeDate = dateString
        }
        submitModel.save()
        this.finish()
    }

    private fun getSelectedQuestion(): RealmList<Int>{
        val questionIds = RealmList<Int>()
        mQuestionList.forEach {
            if(it.isSelected){
                questionIds.add(it.id)
            }
        }
        return questionIds
    }

    private fun getCompleteDate(): String {

        val calendar = Calendar.getInstance()
        val date = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)
        return "$date-$month-$year"
    }

    private fun getScore(): Int{
        var score = 0
        mQuestionList.forEach { question ->
            val userAnswer = UserAnswerModel().query { equalTo("questionId",question.id)}.lastOrNull { it.userEmail == mCurrentUserEmail }
            val answerList = AnswerModel().query { equalTo("questionId",question.id) } as? ArrayList
            answerList?.forEach { answer ->
                if(answer.id == userAnswer?.answerId && answer.isCorrect == true && question.isSelected){
                    score += 1
                }
            }
        }
        return score
    }

    private fun getCurrentUser(){
        mCurrentUserEmail = PreferenceUtils.getFromPrefs(this, Constant.CURRENT_USER_EMAIL_KEY,"") as String
    }
}
