package com.vule.toeic365.ui.authorize

import androidx.databinding.ObservableField
import com.vicpin.krealmextensions.queryAll
import com.vule.toeic365.R
import com.vule.toeic365.base.BaseViewModel
import com.vule.toeic365.model.UserModel
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.activity_authorize.view.*

class LoginViewModel : BaseViewModel() {

    var mEmailField = ObservableField("")
    var mPasswordField = ObservableField("")

    var mShowToastConsumer: Consumer<String>? = null
    var mLoginSuccessConsumer: Consumer<String>? = null

    fun loginClicked() {
        if (mEmailField.get().isNullOrEmpty() || mPasswordField.get().isNullOrEmpty()) {
            mShowToastConsumer?.accept("Vui lòng điền đầy đủ thông tin!")
        } else {
            val userList = UserModel().queryAll()
            val user = userList.firstOrNull { it.email == mEmailField.get() }
            if (user != null) {
                if (mPasswordField.get() == user.password) {
                    mLoginSuccessConsumer?.accept("Đăng nhập thành công!")
                }
            } else {
                mShowToastConsumer?.accept("Người dùng không tồn tại!")
            }
        }

    }

    fun onRegisterClicked() {
        mActivityNavigator?.addFragment(R.id.rl_authorize, RegisterFragment.newInstance(), true)
    }
}