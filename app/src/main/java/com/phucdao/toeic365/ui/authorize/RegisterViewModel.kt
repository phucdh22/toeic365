package com.vule.toeic365.ui.authorize

import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import androidx.databinding.ObservableField
import com.vicpin.krealmextensions.createOrUpdate
import com.vicpin.krealmextensions.queryAll
import com.vicpin.krealmextensions.save
import com.vule.toeic365.base.BaseViewModel
import com.vule.toeic365.model.UserModel
import io.reactivex.functions.Consumer

class RegisterViewModel : BaseViewModel(){

    var mErrorUserEmail = ObservableField<String>()
    var mErrorUserPhone = ObservableField<String>()
    var mErrorPassword = ObservableField<String>()
    var mErrorConfirmPassword = ObservableField<String>()
    var mErrorUserName = ObservableField<String>()

    var mSignUpValid = false
    var mCurrentUser: UserModel = UserModel()

    var mShowToastConsumer: Consumer<String>? = null
    var mRegisterSuccessConsumer: Consumer<String>? = null

    fun getUserEmail(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mSignUpValid = when {
                    s.toString().isEmpty() -> {
                        mErrorUserEmail.set("Email empty")
                        false
                    }
                    !isEmailValid(s.toString()) -> {
                        mErrorUserEmail.set("Email invalid")
                        false
                    }
                    else -> {
                        mErrorUserEmail.set(null)
                        true
                    }
                }
                mCurrentUser.email = s.toString()
            }
        }
    }

    fun getUserPassword(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mSignUpValid = if (s.toString().isEmpty()) {
                    mErrorPassword.set("Password empty")
                    false
                } else {
                    mErrorPassword.set(null)
                    true
                }
                mCurrentUser.password = s.toString()
            }
        }
    }

    fun getUserConfirmPassword(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mSignUpValid = when {
                    s.toString().isEmpty() -> {
                        mErrorConfirmPassword.set("ConfirmPassword empty")
                        false
                    }
                    s.toString() != mCurrentUser.password -> {
                        mErrorConfirmPassword.set("Wrong confirm password")
                        false
                    }
                    else -> {
                        mErrorConfirmPassword.set(null)
                        true
                    }
                }
            }
        }
    }

    fun getUserName(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mSignUpValid = if (s.toString().isEmpty()) {
                    mErrorUserName.set("Name empty")
                    false
                } else {
                    mErrorUserName.set(null)
                    true
                }
                mCurrentUser.name = s.toString()
            }
        }
    }

    fun getUserPhone(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                mSignUpValid = if (s.toString().isEmpty()) {
                    mErrorUserPhone.set("Phone empty")
                    false
                } else {
                    mErrorUserPhone.set(null)
                    true
                }
                mCurrentUser.phone = s.toString()
            }
        }
    }

    fun signUpClicked() {
        if(mSignUpValid) {
            val userList = UserModel().queryAll()
            val shouldAddUser = userList.none { it.email == mCurrentUser.email }
            if (shouldAddUser){
                mRegisterSuccessConsumer?.accept("Đăng ký thành công!")
            } else {
                mShowToastConsumer?.accept("Người dùng đã tồn tại!")
            }
        } else {
            mShowToastConsumer?.accept("Vui lòng điền đầy đủ thông tin!")
        }
    }

    fun onLoginClicked(){
        mActivityNavigator?.popFragment()
    }

    private fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}