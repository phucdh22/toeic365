package com.vule.toeic365.ui.subfunction

import androidx.databinding.ObservableField
import com.vule.toeic365.base.BaseViewModel
import io.reactivex.functions.Consumer

class FunctionViewModel : BaseViewModel(){

    var mIsSubmitVisible = ObservableField(false)
    var mShowToastConsumer: Consumer<String>? = null
    var mShowConfirmDialog: Consumer<Unit>? = null

    fun onBackClicked(){
        if(mIsSubmitVisible.get() == true){
            mIsSubmitVisible.set(false)
        } else {
            mShowConfirmDialog?.accept(Unit)
        }
    }


    fun openSubmitView(){
        mIsSubmitVisible.set(!(mIsSubmitVisible.get() ?: false))
    }

    fun onSubmitClicked(){
        mShowToastConsumer?.accept("Submit!")
    }
}