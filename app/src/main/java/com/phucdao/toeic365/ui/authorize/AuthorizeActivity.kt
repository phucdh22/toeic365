package com.vule.toeic365.ui.authorize

import com.vule.toeic365.BR
import com.vule.toeic365.R
import com.vule.toeic365.base.BaseBindingModelActivity
import com.vule.toeic365.databinding.ActivityAuthorizeBinding

class AuthorizeActivity : BaseBindingModelActivity<ActivityAuthorizeBinding,AuthorizeViewModel>(){

    override fun layoutId(): Int = R.layout.activity_authorize
    override fun viewModelClass(): Class<AuthorizeViewModel> = AuthorizeViewModel::class.java
    override fun bindingVariable(): Int = BR.viewModel

    override fun setupView() {
        super.setupView()
        mViewModel.mActivityNavigator?.addFragment(R.id.rl_authorize,LoginFragment.newInstance(),false)
    }
}
