package com.vule.toeic365.ui.authorize

import com.vicpin.krealmextensions.createOrUpdate
import com.vule.toeic365.BR
import com.vule.toeic365.R
import com.vule.toeic365.base.BaseBindingModelFragment
import com.vule.toeic365.databinding.FragmentRegisterBinding
import com.vule.toeic365.utils.DialogUtils
import io.reactivex.functions.Consumer

class RegisterFragment : BaseBindingModelFragment<FragmentRegisterBinding, RegisterViewModel>() {

    override fun layoutId(): Int = R.layout.fragment_register
    override fun viewModelClass(): Class<RegisterViewModel> = RegisterViewModel::class.java
    override fun bindingVariable(): Int = BR.viewModel

    override fun setupView() {
        super.setupView()
        this.getUserInfo()
        mViewModel.mShowToastConsumer = Consumer { message ->
            DialogUtils.showToastDialog(mActivity, message)
        }
        mViewModel.mRegisterSuccessConsumer = Consumer { message ->
            DialogUtils.showToastDialog(mActivity,message)
            mViewModel.mCurrentUser.createOrUpdate()
            mViewModel.mActivityNavigator?.popFragment()
        }
    }

    private fun getUserInfo(){
        mLayoutBinding.etEmail.addTextChangedListener(mViewModel.getUserEmail())
        mLayoutBinding.etPassword.addTextChangedListener(mViewModel.getUserPassword())
        mLayoutBinding.etRetypePassword.addTextChangedListener(mViewModel.getUserConfirmPassword())
        mLayoutBinding.etName.addTextChangedListener(mViewModel.getUserName())
        mLayoutBinding.etMobileNumber.addTextChangedListener(mViewModel.getUserPhone())
    }

    companion object {
        fun newInstance(): RegisterFragment {
            return RegisterFragment()
        }
    }
}