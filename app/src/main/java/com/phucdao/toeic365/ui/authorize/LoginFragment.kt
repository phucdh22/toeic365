package com.vule.toeic365.ui.authorize

import android.content.Intent
import com.vule.toeic365.BR
import com.vule.toeic365.R
import com.vule.toeic365.base.BaseBindingModelFragment
import com.vule.toeic365.constant.Constant
import com.vule.toeic365.databinding.FragmentLoginBinding
import com.vule.toeic365.ui.home.HomeActivity
import com.vule.toeic365.utils.DialogUtils
import com.vule.toeic365.utils.PreferenceUtils
import io.reactivex.functions.Consumer

class LoginFragment : BaseBindingModelFragment<FragmentLoginBinding,LoginViewModel>(){

    override fun layoutId(): Int = R.layout.fragment_login
    override fun viewModelClass(): Class<LoginViewModel> = LoginViewModel::class.java
    override fun bindingVariable(): Int = BR.viewModel

    override fun setupView() {
        super.setupView()
        mViewModel.mShowToastConsumer = Consumer {message ->
            DialogUtils.showToastDialog(mActivity,message)
        }
        mViewModel.mLoginSuccessConsumer = Consumer { message ->
            PreferenceUtils.saveToPrefs(requireContext(),Constant.CURRENT_USER_EMAIL_KEY,mViewModel.mEmailField.get() ?: "")
            DialogUtils.showToastDialog(mActivity,message)
            startActivity(Intent(mActivity,HomeActivity::class.java))
            mActivity?.finish()
        }
    }

    companion object{
        fun newInstance(): LoginFragment{
            return LoginFragment()
        }
    }
}