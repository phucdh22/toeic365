package com.vule.toeic365.ui.home

import androidx.databinding.ObservableField
import com.vule.toeic365.base.BaseViewModel
import com.vule.toeic365.model.UserModel
import io.reactivex.functions.Consumer

class HomeViewModel : BaseViewModel(){

    var mCurrentUser: UserModel? = null

    var mUserNameField = ObservableField("")
    var mUserEmailField = ObservableField("")

    var mShowNavigatorConsumer: Consumer<Boolean>? = null
    var mShowToastConsumer: Consumer<String>? = null
    var mLogoutConsumer: Consumer<Unit>? = null
    var mOnPartClickedConsumer: Consumer<Int>? = null

    var isNavigatorShowing = true

    fun onNavigatorClicked(){
        mShowNavigatorConsumer?.accept(true)
    }

    fun onCloseClicked(){
       mShowNavigatorConsumer?.accept(false)
    }

    fun onBeginnerClicked(){
        mOnPartClickedConsumer?.accept(1)
    }

    fun onIntermediateClicked(){
        mOnPartClickedConsumer?.accept(2)
    }

    fun onAdvancedClicked(){
        mShowToastConsumer?.accept("Advanced")
    }

    fun onProfessionClicked(){
        mShowToastConsumer?.accept("Profession")
    }

    fun onExpertClicked(){
        mShowToastConsumer?.accept("Expert")
    }

    fun onPart6Clicked(){
        mShowToastConsumer?.accept("Part 6")
    }

    fun onLogoutClicked(){
        mLogoutConsumer?.accept(Unit)
    }

    fun updateUserData(){
         mUserNameField.set(mCurrentUser?.name)
        mUserEmailField.set(mCurrentUser?.email)
    }
}