package com.vule.toeic365.ui.home

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.RelativeLayout
import com.vicpin.krealmextensions.*
import com.vule.toeic365.BR
import com.vule.toeic365.R
import com.vule.toeic365.adapter.PracticeAdapter
import com.vule.toeic365.base.BaseBindingModelActivity
import com.vule.toeic365.constant.Constant
import com.vule.toeic365.databinding.ActivityMainBinding
import com.vule.toeic365.listener.PracticeListener
import com.vule.toeic365.model.PracticeModel
import com.vule.toeic365.model.SubmitModel
import com.vule.toeic365.model.UserModel
import com.vule.toeic365.ui.authorize.AuthorizeActivity
import com.vule.toeic365.ui.subfunction.FunctionActivity
import com.vule.toeic365.utils.DialogUtils
import com.vule.toeic365.utils.PreferenceUtils
import com.vule.toeic365.utils.Utils
import io.reactivex.functions.Consumer

class HomeActivity : BaseBindingModelActivity<ActivityMainBinding, HomeViewModel>(),
    PracticeListener {

    private lateinit var mPracticeList: ArrayList<PracticeModel>
    override fun layoutId(): Int = R.layout.activity_main
    override fun viewModelClass(): Class<HomeViewModel> = HomeViewModel::class.java
    override fun bindingVariable(): Int = BR.viewModel

    override fun onPlayClicked(position: Int) {
        PreferenceUtils.saveToPrefs(this,Constant.CURRENT_PRACTICE_ID,mPracticeList[position].id ?: 0)
        startActivity(Intent(this,FunctionActivity::class.java))
    }

    override fun onReplayClicked(position: Int) {
        val practice = mPracticeList[position]
        SubmitModel().delete { equalTo("id",mViewModel.mCurrentUser?.email + practice.id) }
        initRecyclerView()
    }

    override fun onReviewClicked(position: Int) {
        startActivity(Intent(this,FunctionActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        PreferenceUtils.saveToPrefs(this,Constant.CURRENT_PART_ID,1)
    }

    override fun onResume() {
        super.onResume()
        animateNavigatorView(false)
        getCurrentUser()
        initRecyclerView()
        subscribeListener()
    }
    @SuppressLint("ClickableViewAccessibility")
    private fun initRecyclerView(){
        val partId = PreferenceUtils.getFromPrefs(this,Constant.CURRENT_PART_ID,1) as Int
        mPracticeList = PracticeModel().query { equalTo(Constant.mPartIdField,partId) } as ArrayList
        mLayoutBinding.rvPractice.adapter = PracticeAdapter(mPracticeList, this)
        mLayoutBinding.rvPractice.setOnTouchListener { _, _ ->
            if(mViewModel.isNavigatorShowing) animateNavigatorView(false)
            false
        }
    }

    private fun subscribeListener(){
        mViewModel.mShowNavigatorConsumer = Consumer { isShow ->
            animateNavigatorView(isShow)
        }
        mViewModel.mShowToastConsumer = Consumer { message ->
            DialogUtils.showToastDialog(this, message)
        }
        mViewModel.mLogoutConsumer = Consumer {
            PreferenceUtils.saveToPrefs(this,Constant.CURRENT_USER_EMAIL_KEY,"")
            startActivity(Intent(this,AuthorizeActivity::class.java))
            this.finish()
        }
        mViewModel.mOnPartClickedConsumer = Consumer {partId ->
            animateNavigatorView(false)
            PreferenceUtils.saveToPrefs(this,Constant.CURRENT_PART_ID,partId)
            initRecyclerView()
        }
    }

    private fun animateNavigatorView(isShow: Boolean) {
        if(mViewModel.isNavigatorShowing == isShow) return
        val maxWidth = Utils.getDimension(R.dimen._200sdp)
        val animator = ValueAnimator.ofInt(if (isShow) -maxWidth else 0, if (isShow) 0 else -maxWidth)
        animator.addUpdateListener { valueAnimator ->
            val layoutParams = mLayoutBinding.vNavigator.layoutParams as RelativeLayout.LayoutParams
            layoutParams.marginStart = valueAnimator.animatedValue as Int
            mLayoutBinding.vNavigator.layoutParams = layoutParams
        }
        animator.duration = 200
        animator.start()
        //
        mViewModel.isNavigatorShowing = isShow
    }

    private fun getCurrentUser(){
        val currentUserEmail = PreferenceUtils.getFromPrefs(this,Constant.CURRENT_USER_EMAIL_KEY,"") as String
        mViewModel.mCurrentUser = UserModel().queryFirst { equalTo("email",currentUserEmail) }
        mViewModel.updateUserData()
    }
}