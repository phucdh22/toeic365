package com.vule.toeic365.viewholder

import android.annotation.SuppressLint
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.vicpin.krealmextensions.query
import com.vule.toeic365.adapter.AnswerOfQuestionAdapter
import com.vule.toeic365.databinding.ItemPraticeDetailBinding
import com.vule.toeic365.listener.PracticeDetailListener
import com.vule.toeic365.model.AnswerModel
import com.vule.toeic365.model.QuestionModel
import com.vule.toeic365.model.UserAnswerModel

class PracticeDetailViewHolder(private val binding: ItemPraticeDetailBinding,private val isReviewing: Boolean,private val listener: PracticeDetailListener,private val userEmail: String): RecyclerView.ViewHolder(binding.root){

    @SuppressLint("SetTextI18n")
    fun bindView(position: Int,question: QuestionModel?){
        val answerList = AnswerModel().query { equalTo("questionId",question?.id) } as? ArrayList

        binding.tvQuestion.text = "${question?.id}. ${question?.title}"

        binding.tvExplainDescription.text = question?.description
        binding.tvTranslationDescription.text = question?.translation

        binding.tvExplainTitle.visibility = if(question?.isSelected == true || isReviewing) View.VISIBLE else View.GONE
        binding.tvExplainDescription.visibility = if(question?.isSelected == true || isReviewing) View.VISIBLE else View.GONE

        binding.tvTranslationDescription.visibility = if(!question?.translation.isNullOrEmpty() && (question?.isSelected == true || isReviewing)) View.VISIBLE else View.GONE
        binding.tvTranslationTitle.visibility = if(!question?.translation.isNullOrEmpty()&& (question?.isSelected == true || isReviewing)) View.VISIBLE else View.GONE
        //
        val userAnswer = UserAnswerModel().query { equalTo("questionId",question?.id)}.lastOrNull { it.userEmail == userEmail }
        binding.rvAnswer.adapter = AnswerOfQuestionAdapter(answerList,listener,question?.isSelected ?: false,isReviewing,position ,userAnswer?.answerId ?: 0)
    }
}