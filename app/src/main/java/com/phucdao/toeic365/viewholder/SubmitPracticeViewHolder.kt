package com.vule.toeic365.viewholder

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.vicpin.krealmextensions.query
import com.vule.toeic365.R
import com.vule.toeic365.constant.Constant
import com.vule.toeic365.databinding.ItemSubmitPraticeBinding
import com.vule.toeic365.listener.PracticeSubmitListener
import com.vule.toeic365.model.AnswerModel
import com.vule.toeic365.model.QuestionModel
import com.vule.toeic365.model.UserAnswerModel
import com.vule.toeic365.utils.PreferenceUtils

class SubmitPracticeViewHolder(private val binding: ItemSubmitPraticeBinding,private val isReviewing: Boolean,private val listener: PracticeSubmitListener): RecyclerView.ViewHolder(binding.root){
    fun bindView(question: QuestionModel?,position: Int){
        binding.tvQuestion.text = (adapterPosition + 1).toString()
        binding.root.setOnClickListener {
            listener.onQuestionClicked(position)
        }
        val userEmail = PreferenceUtils.getFromPrefs(binding.root.context,Constant.CURRENT_USER_EMAIL_KEY,"")
        val answerList = AnswerModel().query { equalTo("questionId",question?.id) } as? ArrayList
        val userAnswer = UserAnswerModel().query { equalTo("questionId",question?.id)}.lastOrNull { it.userEmail == userEmail }
        var cardBackgroundColor = if(isReviewing) R.color.black_4a_03 else R.color.colorPrimary
        if(question?.isSelected == true){
            val currentAnswer = answerList?.firstOrNull { it.id == userAnswer?.answerId }
            cardBackgroundColor = if(currentAnswer?.isCorrect == true) R.color.blue_ff else R.color.colorAccent
        }
        binding.card.setCardBackgroundColor(ContextCompat.getColor(binding.root.context,cardBackgroundColor))
    }
}