package com.vule.toeic365.viewholder

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.vule.toeic365.R
import com.vule.toeic365.databinding.ItemAnswerOfQuestionBinding
import com.vule.toeic365.listener.PracticeDetailListener
import com.vule.toeic365.model.AnswerModel

class AnswerOfQuestionViewHolder(
    private val binding: ItemAnswerOfQuestionBinding,
    private val listener: PracticeDetailListener,
    private val isSelected: Boolean = false,
    private val isReviewing: Boolean,
    private val answerId: Int,
    private val questionPos: Int
) : RecyclerView.ViewHolder(binding.root) {

    fun bindView(answer: AnswerModel?, pos: Int) {
        binding.tvAnswer.text = answer?.title
        var backgroundColor = R.color.white

        if(isSelected){
            if (answer?.id == answerId && answer.isCorrect == false) {
                backgroundColor = R.color.colorAccent
            }
            if (answer?.isCorrect == true ) {
                backgroundColor = R.color.blue_ff
            }
        } else {
            if(isReviewing){
                if (answer?.isCorrect == true ) {
                    backgroundColor = R.color.colorAccent
                }
            }
        }

        binding.tvAnswer.setBackgroundColor(
            ContextCompat.getColor(
                binding.root.context,
                backgroundColor
            )
        )
        binding.tvAnswer.setOnClickListener {
            if (!isSelected && !isReviewing) listener.onUserAnswer(
                questionPos,
                answer?.questionId ?: 0,
                answer?.id ?: 0
            )
        }
        binding.vIndicator.visibility = if (pos == 3) View.GONE else View.VISIBLE
    }
}