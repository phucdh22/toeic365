package com.vule.toeic365.viewholder

import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.vicpin.krealmextensions.query
import com.vule.toeic365.R
import com.vule.toeic365.constant.Constant
import com.vule.toeic365.databinding.ItemPracticeBinding
import com.vule.toeic365.listener.PracticeListener
import com.vule.toeic365.model.PracticeModel
import com.vule.toeic365.model.SubmitModel
import com.vule.toeic365.utils.PreferenceUtils

class PracticeViewHolder(private val binding: ItemPracticeBinding, private val listener: PracticeListener): RecyclerView.ViewHolder(binding.root){

    fun bindView(practice: PracticeModel?, pos: Int){

        val currentUserEmail = PreferenceUtils.getFromPrefs(binding.root.context,Constant.CURRENT_USER_EMAIL_KEY,"") as String
        val submittedPractice = SubmitModel().query { equalTo(Constant.mPracticeIdField,practice?.id) }.lastOrNull { it.userEmail ==  currentUserEmail}
        val isEnableStart = submittedPractice == null

        val startTintColor = ContextCompat.getColor(binding.root.context,if(isEnableStart) R.color.colorPrimary else R.color.black_4a_03)
        val reStartTintColor = ContextCompat.getColor(binding.root.context,if(isEnableStart) R.color.black_4a_03 else R.color.colorPrimary)
        binding.btnStart.setColorFilter(startTintColor)
        binding.btnRestart.setColorFilter(reStartTintColor)
        binding.btnReview.setColorFilter(reStartTintColor)

        binding.btnStart.setOnClickListener {
            if(isEnableStart) listener.onPlayClicked(pos)
        }
        binding.btnRestart.setOnClickListener {
            if(!isEnableStart)listener.onReplayClicked(pos)
        }
        binding.btnReview.setOnClickListener {
            if(!isEnableStart)listener.onReviewClicked(pos)
        }

        binding.tvPracticeTitle.text = practice?.name

        binding.tvScore.text = (submittedPractice?.score ?: 0).toString()
        binding.tvCompleteTime.text = submittedPractice?.completeDate ?: ""
        binding.tvTime.text = submittedPractice?.time ?: "0:00"
    }
}