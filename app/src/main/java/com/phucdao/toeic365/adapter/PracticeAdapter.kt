package com.vule.toeic365.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vule.toeic365.R
import com.vule.toeic365.databinding.ItemPracticeBinding
import com.vule.toeic365.listener.PracticeListener
import com.vule.toeic365.model.PracticeModel
import com.vule.toeic365.viewholder.PracticeViewHolder

class PracticeAdapter(private val dataList: ArrayList<PracticeModel>?,private val listener: PracticeListener): RecyclerView.Adapter<PracticeViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PracticeViewHolder {
        val binding: ItemPracticeBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_practice,parent,false)
        return PracticeViewHolder(binding,listener)
    }

    override fun getItemCount(): Int = dataList?.size ?: 15

    override fun onBindViewHolder(holder: PracticeViewHolder, position: Int) {
        holder.bindView(dataList?.get(position),position)
    }

}