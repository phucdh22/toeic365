package com.vule.toeic365.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vule.toeic365.R
import com.vule.toeic365.databinding.ItemAnswerOfQuestionBinding
import com.vule.toeic365.listener.PracticeDetailListener
import com.vule.toeic365.model.AnswerModel
import com.vule.toeic365.viewholder.AnswerOfQuestionViewHolder

class AnswerOfQuestionAdapter (private val dataList: ArrayList<AnswerModel>?,
                               private val listener: PracticeDetailListener,
                               private val isSelected: Boolean = false,
                               private val isReviewing: Boolean,
                               private val questionPos: Int,
                               private val answerId: Int): RecyclerView.Adapter<AnswerOfQuestionViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswerOfQuestionViewHolder {
        val binding: ItemAnswerOfQuestionBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_answer_of_question,parent,false)
        return AnswerOfQuestionViewHolder(binding,listener,isSelected,isReviewing,answerId,questionPos)
    }

    override fun getItemCount(): Int = dataList?.size ?: 0

    override fun onBindViewHolder(holder: AnswerOfQuestionViewHolder, position: Int) {
        holder.bindView(dataList?.get(position),position)
    }

}