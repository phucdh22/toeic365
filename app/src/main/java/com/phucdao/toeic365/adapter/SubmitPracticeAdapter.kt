package com.vule.toeic365.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vule.toeic365.R
import com.vule.toeic365.databinding.ItemSubmitPraticeBinding
import com.vule.toeic365.listener.PracticeSubmitListener
import com.vule.toeic365.model.QuestionModel
import com.vule.toeic365.viewholder.SubmitPracticeViewHolder

class SubmitPracticeAdapter(private val dataList: ArrayList<QuestionModel>?,private val isReviewing: Boolean,private val listener: PracticeSubmitListener) : RecyclerView.Adapter<SubmitPracticeViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubmitPracticeViewHolder {
        val binding: ItemSubmitPraticeBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_submit_pratice,parent,false)
        return SubmitPracticeViewHolder(binding,isReviewing,listener)
    }

    override fun getItemCount(): Int = dataList?.size ?: 0

    override fun onBindViewHolder(holder: SubmitPracticeViewHolder, position: Int) {
        holder.bindView(dataList?.get(position),position)
    }

}