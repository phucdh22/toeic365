package com.vule.toeic365.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.vule.toeic365.R
import com.vule.toeic365.databinding.ItemPracticeBinding
import com.vule.toeic365.databinding.ItemPraticeDetailBinding
import com.vule.toeic365.listener.PracticeDetailListener
import com.vule.toeic365.model.QuestionModel
import com.vule.toeic365.viewholder.PracticeDetailViewHolder

class PracticeDetailAdapter(private val dataList: ArrayList<QuestionModel>?,private val isReviewing: Boolean,private val listener: PracticeDetailListener,private val userEmail: String) : RecyclerView.Adapter<PracticeDetailViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PracticeDetailViewHolder {
        val binding: ItemPraticeDetailBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.item_pratice_detail,parent,false)
        return PracticeDetailViewHolder(binding,isReviewing,listener,userEmail)
    }

    override fun getItemCount(): Int  = dataList?.size ?: 0

    override fun onBindViewHolder(holder: PracticeDetailViewHolder, position: Int) {
        holder.bindView(position,dataList?.get(position))
    }

}