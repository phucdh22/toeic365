package com.vule.toeic365.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class SubmitModel : RealmObject(){

    @PrimaryKey
    @SerializedName("id")
    var id: String? = null
    @SerializedName("userEmail")
    var userEmail: String? = null

    @SerializedName("practiceId")
    var practiceId: Int? = null

    @SerializedName("time")
    var time: String? = null

    @SerializedName("score")
    var score: Int? = null

    @SerializedName("selectedQuestionId")
    var selectedQuestionId: RealmList<Int>? = null

    @SerializedName("completeDate")
    var completeDate: String? = null
}