package com.vule.toeic365.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class UserModel: RealmObject(){

    @PrimaryKey
    @SerializedName("id")
    var id: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("password")
    var password: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("avatar")
    var avatar: String? = null

    @SerializedName("name")
    var name: String? = null
}