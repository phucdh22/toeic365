package com.vule.toeic365.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class QuestionModel : RealmObject(){

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("practiceId")
    var practiceId: Int? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("translation")
    var translation: String? = null

    @SerializedName("isSelected")
    var isSelected: Boolean = false

    fun initData(id: Int,title: String, practiceId: Int,description: String,translation: String): QuestionModel{
        this.id = id
        this.title = title
        this.practiceId = practiceId
        this.description = description
        this.translation = translation
        return this
    }
}