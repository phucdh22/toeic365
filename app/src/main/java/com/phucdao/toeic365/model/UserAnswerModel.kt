package com.vule.toeic365.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

open class UserAnswerModel : RealmObject(){

    @SerializedName("userEmail")
    var userEmail: String? = null

    @SerializedName("partId")
    var partId: Int? = null

    @SerializedName("practiceId")
    var practiceId: Int? = null

    @SerializedName("questionId")
    var questionId: Int? = null

    @SerializedName("answerId")
    var answerId: Int? = null
}