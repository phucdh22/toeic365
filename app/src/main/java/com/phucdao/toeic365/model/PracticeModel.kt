package com.vule.toeic365.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class PracticeModel : RealmObject(){
    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("partId")
    var partId: Int? = null

    fun initData(id: Int,name: String,partId: Int): PracticeModel {
        this.id = id
        this.name = name
        this.partId = partId
        return this
    }
}