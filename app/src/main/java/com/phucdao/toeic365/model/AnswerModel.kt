package com.vule.toeic365.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class AnswerModel : RealmObject(){

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("isCorrect")
    var isCorrect: Boolean? = false

    @SerializedName("questionId")
    var questionId: Int? = null
}