package com.vule.toeic365.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class PartModel : RealmObject() {

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    fun initData(id: Int, name: String): PartModel {
        this.id = id
        this.name = name
        return this
    }
}