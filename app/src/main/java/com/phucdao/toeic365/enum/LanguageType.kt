package com.vule.toeic365.enum

enum class LanguageType(val value: String) {
    VN("vi"),
    EN("en")
}